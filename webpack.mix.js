let mix = require("laravel-mix");

Config.autoprefixer = false;

mix
    .postCss("resources/assets/css/custom.css", "public/css", [
        require("postcss-cssnext")()
    ])
    .js("resources/assets/js/app.js", "public/js");
