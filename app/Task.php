<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\Cast\Array_;

class Task extends Model
{
    protected $with=['versions','type','resource','status'];
    protected $guarded=[];

    public function requirement()
    {
        return $this->belongsTo('App\Requirement','requirement_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Type','type_id');
    }

    public function resource()
    {
        return $this->belongsTo('App\Resource','resource_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status','status_id');
    }

    public function versions()
    {
        return $this->hasMany('App\Task_Version','task_id');
    }

    public function lastVersion()
    {
        return $this->versions->sortByDesc('version')->first();
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }

    

}
