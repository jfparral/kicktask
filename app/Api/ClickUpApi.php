<?php
namespace App\Api;

use App\Task;
use Carbon\Carbon;
use App\StatusType;

use Illuminate\Http\Request;

class ClickUpApi{

    /* Methods by Clickup */
    /*  id team :  451927
        id space Kickads : 452491
        id space Diseño : 453840
        list for Diseño 
              "lists": [
        {
          "id": "468744",
          "name": "BackEnd"
        },
        {
          "id": "468745",
          "name": "FrontEnd"
        },
        {
          "id": "468746",
          "name": "Fixes"
        }
      ]

        id project Kicktask: 458186 
    */

    public function getSpaces()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.clickup.com/api/v1/team/451927/space");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: pk_76X42WTJJP6CB6M2CVUW0Q110C4MJKEN"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $tmp=json_decode($response,true);

        return $tmp['spaces'];
    }

    public function getProjects(Task $task)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.clickup.com/api/v1/space/{$task->resource->space_id}/project");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: pk_76X42WTJJP6CB6M2CVUW0Q110C4MJKEN"
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $tmp=json_decode($response,true);

        return $tmp['projects'];
    }

    public function getUsers()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.clickup.com/api/v1/team");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: pk_76X42WTJJP6CB6M2CVUW0Q110C4MJKEN"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $clickup_users=json_decode($response,true);
        //dd($clickup_users);
        
        return $clickup_users['teams'][0]['members'];
    }

    public function getTasks()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.clickup.com/api/v1/team/451927/task");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: pk_76X42WTJJP6CB6M2CVUW0Q110C4MJKEN"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $tmp=json_decode($response,true);
        $tasks=$tmp['tasks'];
        
        return $tasks;
    }

    public function postTask(Request $request, Task $task)
    {
        $time=new Carbon($task->due_date);
        $date=$time->timestamp;
        $ch = curl_init();
        $list_id=$request->list_id;
        curl_setopt($ch, CURLOPT_URL, "https://api.clickup.com/api/v1/list/{$list_id}/task");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, "{
        \"name\": \"{$task->client->name}-{$task->name}-V{$task->lastversion()->version}\",
        \"content\": \"{$task->lastVersion()->description}\",
        \"assignees\": [
            {$request->user_id}
        ],
        \"status\": \"Open\",
        \"priority\": 3,
        \"due_date\": \"{$date}123\"
        }");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: pk_76X42WTJJP6CB6M2CVUW0Q110C4MJKEN",
        "Content-Type: application/json"
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        
        $tmp=json_decode($response,true);
        $status = StatusType::where('type', 'tasks')->get()->map(function ($type) {
            return $type->status;
        })->where('name','Enviada')->toArray();

        $task->clickup_id=$tmp['id'];
        $task->status_id=$status[4]['id'];
        $task->update();
        
        return redirect()->route('tasks.index')->with('success','La tarea ha sido subida a clickup.');
    }

}
