<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $with = [];

    public function getLogoAttribute($value)
    {
        return asset("storage/{$value}");
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'client_users', 'client_id', 'user_id');
    }

    public function requirements()
    {
        return $this->hasMany('App\Requirement','client_id');
    }
}
