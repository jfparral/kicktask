<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public function contact()
    {
        return $this->belongsTo('App\User','provider_id');
    }

    public function quote_detail()
    {
        return $this->belongsTo('App\Quote_Detail','quote_detail_id');
    }

    public function quote()
    {
        return $this->belongsTo('App\Quote','quote_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
}
