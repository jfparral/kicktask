<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Quotes_DetailsController;

class Quotes_Detail extends Model
{
    protected $guarded = [];
    protected $table = 'quotes_details';
    protected $with = ['dealer', 'resource','typeTask'];

    public function quote()
    {
        return $this->belongsTo('App\Quote', 'quote_id');
    }

    public function resource()
    {
        return $this->belongsTo('App\Resource', 'resource_id');
    }

    public function typeTask()
    {
        return $this->belongsTo('App\Type', 'type_id');
    }

    public function dealer()
    {
        return $this->belongsTo('App\User', 'dealer_id');
    }

    /**
     * Compare base on array
     *
     * @param [type] $array
     * @return boolean
     */
    public function Comparator($array)
    {
        $tmp=collect($this)->only(array_keys($array))->toArray();
        $result=array_diff_assoc($tmp,$array);
        return count($result)>0;
        /* return collect($this)
            ->only(array_keys($array))
            ->reduce(function ($total, $value) use ($array) {
                return $total + (int)!in_array($value, $array);
            }, 0) == 0; */
    }

}
