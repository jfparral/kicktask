<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $guarded=[];
    protected $with=['client','contact','user','status'];

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function contact()
    {
        return $this->belongsTo('App\User', 'contact_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function quotesDetail()
    {
        return $this->hasMany('App\Quotes_Detail','quote_id');
    }

    public function lastVersion()
    {
        return $this->quotesDetail->sortByDesc('version')->latest()->first();
    }

}
