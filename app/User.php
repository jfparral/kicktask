<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $with = ['role'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_users', 'user_id', 'client_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function hasClient($id)
    {
        return in_array($id, $this->clients->pluck('id')->toArray());
    }

    public function contact()
    {
        return $this->hasMany('App\User','contact_id');
    }
}
