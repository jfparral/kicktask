<?php

namespace App\Http\Controllers;

use App\Type;
use App\User;
use App\Client;
use App\Source;
use App\Status;
use App\Resource;
use App\Requirement;
use Illuminate\Http\Request;

class RequirementsController extends Controller
{
    public function index(Request $request)
    {
        $clients = Client::all();
        $sources = Source::all();
        $requirements = Requirement::when($request->filled('client_id'), function ($query) use ($request) {
            return $query->where('client_id', $request->input('client_id'));
        })->when($request->filled(['from', 'to']), function ($query) use ($request) {
            return $query->whereBetween('due_date', [$request->input('from'), $request->input('to')]);
        })->when($request->filled('source_id'),function ($query) use ($request){
            return $query->where('source_id', $request->input('source_id'));
        })->get();
        return view('requirements.index', compact('requirements', 'clients', 'sources'));
    }

    public function show(Requirement $requirement)
    {
        $resources = Resource::all();
        $types = Type::all();
        $statuses = Status::all();
        return view('requirements.show', compact('requirement', 'resources', 'types', 'statuses'));
    }

    public function create()
    {
        $clients = Client::all();
        $contacts = User::all();
        $sources = Source::all();
        return view('requirements.create', compact('clients', 'contacts', 'sources'));
    }

    public function store(Request $request)
    {
        $messages = [
            'client_id.required' => 'Escoja un cliente.',
            'client_id.exists' => 'Escoja un cliente.',
            'contact_id.required' => 'Escoja un contacto.',
            'contact_id.exists' => 'Escoja un contacto.',
            'source_id.required' => 'Escoja una fuente.',
            'source_id.exists' => 'Escoja una fuente.',
            'due_date.required' => 'Escoja una fecha.',
            'description.required' => 'Rellene la descripción.'
        ];

        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'contact_id' => 'required|exists:users,id',
            'due_date' => 'required',
            'description' => 'required',
            'source_id' => 'required'
        ], $messages);

        $requirement = new Requirement($request->all());
        $requirement->user_id = 2;
        $requirement->status_id = 1;
        $requirement->save();

        return redirect()->route('requirements.index')->with('success', 'La solicitud ha sido ingresada exitosamente.');
    }

    public function edit(Requirement $requirement)
    {
        $clients = Client::all();
        $contacts = User::all();
        $sources = Source::all();
        return view('requirements.edit', compact('requirement', 'clients', 'contacts', 'sources'));
    }

    public function update(Request $request, Requirement $requirement)
    {
        $messages = [
            'client_id.required' => 'Escoja un cliente.',
            'client_id.exists' => 'Escoja un cliente.',
            'contact_id.required' => 'Escoja un contacto.',
            'contact_id.exists' => 'Escoja un contacto.',
            'source_id.required' => 'Escoja una fuente.',
            'source_id.exists' => 'Escoja una fuente.',
            'due_date.required' => 'Escoja una fecha.',
            'description.required' => 'Rellene la descripción.'
        ];

        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'contact_id' => 'required|exists:users,id',
            'due_date' => 'required',
            'description' => 'required',
            'source_id' => 'required'
        ], $messages);

        $requirement->update($request->all());

        return redirect()->route('requirements.index')->with('success', 'La solicitud ha sido actualizada correctamente.');
    }

    public function destroy(Requirement $requirement)
    {
        $requirement->delete();

        return redirect()->route('requirements.index')->with('success', 'La solicituda ha sido eliminada correctamente.');
    }
}
