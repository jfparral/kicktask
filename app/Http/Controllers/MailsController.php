<?php

namespace App\Http\Controllers;
use Mail;
use App\Task;
use App\Quote;
use App\Status;
use App\Provider;
use App\Task_Version;
use App\Quotes_Detail;
use App\Jobs\EmailsJob;
use App\Mail\QuotesEmail;
use App\Mail\AcceptMail;
use App\Mail\RefuseMail;
use Illuminate\Http\Request;

class MailsController extends Controller
{
    public function send(Quote $quote)
    {
        Mail::to($quote->contact->email)
            ->send(new QuotesEmail($quote));
        return redirect()->route('quotes.index')->with('success','La cotización ha sido enviado exitosamente.');

    }

    public function view($token,$id)
    {
        $quote = Quote::where('id', $id)->first();

        if($quote->email_token == $token){

            $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
                return $group->sortByDesc('version')->first();
            });

            $quotes_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
                return $group->sortByDesc('version')->first();
            });

            $iva_value = 12;

            $subtotal = 0;

            foreach ($quotes_details_intern as $quote_detail) {
                $subtotal = $subtotal + $quote_detail->total;
            }

            foreach ($quotes_details_extern as $quote_detail) {
                $subtotal = $subtotal + $quote_detail->total;
            }

            $iva = round((($subtotal * $iva_value) / 100) * 100) / 100;

            $total = number_format(round(($subtotal + $iva) * 100) / 100, 2, '.', '');

            return view('email.viewquotes',compact('quote', 'iva','subtotal','total' ,'quotes_details_intern', 'quotes_details_extern'));
        }

        return view('email.404email');
    }

    public function exit(){
        return view('email.quoteproccess');
    }

    public function accept($token){
        $quote = Quote::where('email_token', $token)->first();

        if($quote != null){

            
            $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
                return $group->sortByDesc('version')->first();
            });

            $quote_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
                return $group->sortByDesc('version')->first();
            });
            
            foreach ($quotes_details_intern as $quote_detail) {
                $task=new Task();
                $task->name=$quote_detail->name;
                $task->status_id = 1;
                $task->resource_id = $quote_detail->resource_id;
                $task->client_id = $quote->client_id;
                $task->requirement_id = $quote->requirement_id;
                $task->quote_id = $quote->id;
                $task->type_id = $quote_detail->type_id;
                $task->save();

                $task_version = new Task_Version();
                $task_version->description = $quote_detail->description;
                $task_version->task_id = $task->id;
                $task_version->version = 1;
                $task_version->save();
            }
            foreach ($quote_details_extern as $quote_detail){
                $provider=new Provider();
                $provider->name_provider = $quote_detail->dealer->name;
                $provider->description = $quote_detail->description;
                $provider->price = $quote_detail->total;
                $provider->status_id = 1;
                $provider->quote_id = $quote->id;
                $provider->provider_id = $quote->contact->id;
                $provider->quote_detail_id = $quote_detail->id;
                $provider->save();
            }

            Mail::to($quote->contact->email)
                ->send(new AcceptMail($quote));

            $status = Status::where('name','Aprobada')->first();
            $quote->status_id = $status->id;
            $quote->update();

            return redirect()->route('email.exit')->with('success', 'Cotización procesada exitosamente.');
        }

        return redirect()->route('email.404email');

    }

    public function refuse($token, Request $request)
    {
        $quote = Quote::where('email_token', $token)->first();
        $description= $request->input('description');

        if ($quote != null) {
            Mail::to($quote->contact->email)
                ->send(new RefuseMail($quote,$request));

            $status = Status::where('name', 'Rechazada')->first();
            $quote->status_id = $status->id;
            $quote->update();

            return redirect()->route('email.exit')->with('success', 'Cotización procesada exitosamente.');
        }

        return redirect()->route('email.404email');
    }
}
