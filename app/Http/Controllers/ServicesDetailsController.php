<?php

namespace App\Http\Controllers;

use App\Client;
use App\Service;
use App\Services_detail;
use Illuminate\Http\Request;

class ServicesDetailsController extends Controller
{
    public function update(Services_detail $service_detail, Request $request)
    {
        $message = [
            'name.required' => 'Nombre del subservicio requerido.',
            'description.required' => 'Descripción requerida.',
            'hours.required' => 'Horas de trabajo requeridas.'
        ];

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'hours' => 'required'
        ],$message);
        
        $service = Service::find($service_detail->service_id)->first();
        $client = Client::find($service->client_id)->first();
        $service_detail->update($request->all());
        
        return redirect()->route('clients.show',[$client])->with('success','El detalle del servicio fue actualizado.');
    }

    public function destroy(Services_detail $service_detail)
    {
        $service = Service::find($service_detail->service_id)->first();
        $client = Client::find($service->client_id)->first();
        $service_detail->delete();

        return redirect()->route('clients.show',[$client])->with('success','El detalle de servicio ha sido eliminado.');
    }
}
