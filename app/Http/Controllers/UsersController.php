<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $clients = Client::all();

        return view('users.create', compact('roles', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages=[
            'name.required'=>'Nombre requerido.',
            'password.required'=>'Contraseña requerida',
            'password.min'=>'Contraseña debe tener mínimo 6 caracteres.',
            'email.required'=>'Email requerido.',
            'email.email'=>'Error, ejemplo: kicktask@example.com',
            'email.unique'=>'Email ya en uso, ingrese uno nuevo.',
            'phone.required'=>'Teléfono requerido.',
            'legal_id.required'=>'Cédula requerido.',
            'legal_id.numeric'=> 'Cédula debe ser numérico.',
            'legal_id.min'=>'Cédula debe tener mínimo 10 número.',
            'address.required'=>'Dirección requerida.',
            'role_id.required'=>'Seleccione un rol.'
        ];

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'sometimes|required|min:6',
            'phone' => 'sometimes|required',
            'address' => 'sometimes|required',
            'legal_id' => 'sometimes|numeric|min:10',
            'role_id' => 'required|exists:roles,id',
        ],$messages);

        $user = new User($request->except(['clients', 'password']));
        $user->password = bcrypt($request->password);
        $user->save();

        if ($request->has('clients')) {
            $user->clients()->sync($request->clients);
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $client = Client::where('id',$user->client_id);
        $roles = Role::all();

        return view('users.show', compact('user', 'client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $clients = Client::all();
        $roles = Role::all();

        return view('users.edit', compact('user', 'clients', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id)
            ],
            'password' => 'nullable|min:6',
            'phone' => 'sometimes|numeric',
            'legal_id' => 'sometimes|numeric|min:10',
            'role_id' => 'required|exists:roles,id',
        ]);

        $user->fill($request->except(['clients', 'password']));

        if ($request->filled('password')) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        if ($request->has('clients')) {
            $user->clients()->sync($request->clients);
        }

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->clients()->detach(); // ELIMINAR ESTO
        $user->delete();

        return redirect()->route('users.index');
    }
}
