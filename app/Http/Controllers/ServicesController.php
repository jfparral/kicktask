<?php

namespace App\Http\Controllers;
use App\Client, App\Resource, App\Service, App\Services_detail;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index(Client $client)
    {
        return view('services.index');
    }

    public function create(Client $client)
    {
        $resources=Resource::all();
        return view('services.create',compact('client','resources'));
    }

    public function store(Client $client, Request $request)
    {
        $messages = [
            'resource_id.required' => 'Escoja un recurso.',
            'resource_id.exists' => 'Escoja un recurso.',
            'description.required' => 'Detalle requerido.',
            'name.required' => 'Nombre requerido.',
            'hours.required' => 'Horas requerida.'
        ];

        $request->validate([
            'resource_id'=>'required|exists:resources,id',
            'name' => 'required',
            'description' => 'required',
            'hours' => 'required'
        ],$messages);
        
        $service = new Service($request->only('resource_id'));
        $service->client_id = $client->id;
        $service->save();
        $detail_service = new Services_detail($request->only('name','description'));
        $detail_service->hours = $request->input('hours');
        $detail_service->service_id =$service->id;
        $detail_service->save();
        
        return redirect()->route('clients.show',[$client])->with('success', "El servicio ha sido creado correctamente.");
    }


    public function destroy(Service $service)
    {
        $client = Client::find($service->client_id)->first();
        $subservices = Services_detail::where('service_id',$service->id)->get();
        foreach($subservices as $service_detail){
            $service_detail->delete();
        }
        $service->delete();
        return redirect()->route('clients.show',[$client])->with('success', 'El servicio ha sido eliminado exisotamente.');
    }
}
