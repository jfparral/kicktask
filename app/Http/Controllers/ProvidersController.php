<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    public function index()
    {
        $providers=Provider::all();
        return view('providers.index',compact('providers'));
    }

    public function show(Provider $provider)
    {
        return view('providers.show',compact('provider'));
    }

    public function update(Request $request, Provider $provider)
    {
        $message = [
            'due_date.required' => 'Requerida fehca de entrega'
        ];

        $request->validate([
            'due_date' => 'required|date'
        ], $message);

        $provider->due_date = $request->input('due_date');
        $provider->update();
        
        return redirect()->route('providers.index')->with('success','La tarea fue modificada exitosamente.');
    }

    public function destroy(Provider $provider)
    {
        $provider->delete();
        return redirect()->route('providers.index')->with('success', 'La tarea de proveedor ha sido eliminada.');

    }
}
