<?php

namespace App\Http\Controllers;
use PDF;
use App\Quote;
use App\Quotes_Detail;
use App\Task;
use Illuminate\Http\Request;;

class PDFController extends Controller
{

    public function pdf_download($id)
    {
        $quote = Quote::find($id)->first();
        $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        $quotes_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        $iva_value=12;

        $subtotal=0;

        foreach($quotes_details_intern as $quote_detail){
            $subtotal=$subtotal+$quote_detail->total;
        }

        foreach ($quotes_details_extern as $quote_detail) {
            $subtotal = $subtotal + $quote_detail->total;
        }

        $iva = round((($subtotal * $iva_value) / 100) * 100) / 100;

        $total = number_format(round(($subtotal + $iva) * 100) / 100, 2, '.', '');

        $pdf = PDF::loadView('email.pdf', compact('quote', 'iva', 'quotes_details_intern','subtotal','total', 'quotes_details_extern'));
        
        return $pdf->download('quote.pdf');

    }

    public function download($id)
    {
        $quote = Quote::find($id)->first();
        $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        $iva_value = 12;

        $subtotal = 0;

        foreach ($quotes_details_intern as $quote_detail) {
            $subtotal = $subtotal + $quote_detail->total;
        }

        $iva = round((($subtotal * $iva_value) / 100) * 100) / 100;

        $total = number_format(round(($subtotal + $iva) * 100) / 100, 2, '.', '');

        return view('email.pdf',compact('quote', 'iva', 'quotes_details_intern', 'subtotal', 'total'));
    }
}
