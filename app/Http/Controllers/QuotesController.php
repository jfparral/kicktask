<?php

namespace App\Http\Controllers;

use App\Type;
use App\User;
use App\Quote;
use App\Client;
use App\Status;
use App\Resource;
use App\StatusType;
use App\Requirement;
use App\Quotes_Detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuotesController extends Controller
{
    public function index(Request $request)
    {
        $quotes = Quote::when($request->filled('client_id'), function ($query) use ($request) {
            return $query->where('client_id', $request->input('client_id'));
        })->when($request->filled(['from', 'to']), function ($query) use ($request) {
            return $query->whereBetween('exp_date', [$request->input('from'), $request->input('to')]);
        })->when($request->filled('status_id'), function ($query) use ($request) {
            return $query->where('status_id', $request->input('status_id'));
        })->get();
        $statuses = StatusType::where('type', 'quotes')->get()->map(function ($type) {
            return $type->status;
        });
        $clients = Client::all();
        return view('quotes.index', compact('quotes', 'statuses', 'clients'));
    }

    public function store(Request $request, Requirement $requirement)
    {
        $user = Auth::user();
        $quote = new Quote($request->all());
        $quote->status_id = 8;
        $quote->user_id = $user->id;
        $quote->save();
        return redirect()->route('quotes.edit', [$quote]);
    }

    public function show(Quote $quote)
    {
        $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });
        
        $quotes_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });
        
        return view('quotes.show', compact('quote','quotes_details_intern','quotes_details_extern'));
    }

    public function edit(Quote $quote)
    {
        $resources = Resource::all();
        $dealers = User::where('role_id', '4')->get();
        $types = Type::all();
        $statuses = StatusType::where('type', 'quotes')->get()->map(function ($type) {
            return $type->status;
        });

        $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });
        
        $quotes_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        return view('quotes.edit', compact('quote', 'quotes_details_intern', 'quotes_details_extern', 'resources', 'dealers', 'types', 'statuses'));
    }

    public function update(Request $request, Quote $quote)
    {
        $messages = [
            'payment.required' => 'Forma de Pago requerida.',
            'exp_date.required' => 'Fecha de expiración de la cotización requerida.',
            'terms.required' => 'Ingrese los términos legales.'
        ];

        $request->validate([
            'payment' => 'required',
            'exp_date' => 'required',
            'terms' => 'required'
        ], $messages);

        $quotes_details_intern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'intern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        $quotes_details_extern = Quotes_Detail::where('quote_id', $quote->id)->where('type', 'extern')->get()->groupBy('group')->map(function ($group) {
            return $group->sortByDesc('version')->first();
        });

        $iva_value = 12;

        $subtotal = 0;

        foreach ($quotes_details_intern as $quote_detail) {
            $subtotal = $subtotal + $quote_detail->total;
        }

        foreach ($quotes_details_extern as $quote_detail) {
            $subtotal = $subtotal + $quote_detail->total;
        }

        $iva = round((($subtotal * $iva_value) / 100) * 100) / 100;

        $total = number_format(round(($subtotal + $iva) * 100) / 100, 2, '.', '');

        $quote->subtotal = $subtotal;
        $quote->tax = $iva_value;
        $quote->tax_total = $iva;
        $quote->total = $total;

        $quote->update($request->except(['files']));

        return redirect()->route('quotes.index')->with('success', 'La cotización ha sido modificada exitosamente.');
    }

    public function destroy(Quote $quote)
    {
        $quote->delete();
        return redirect()->route('quotes.index')->with('success','La cotización fue eliminada exitosamente.');
    }
}
