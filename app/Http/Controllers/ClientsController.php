<?php

namespace App\Http\Controllers;

use App\Client;
use App\Service;
use App\Services_detail;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        return view('clients.index', compact('clients'));
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(Request $request)
    {
        $messages = [
            'phone.required' => 'Teléfono requerido.',
            'name.required' => 'Nombre requerido.',
            'email.required' => 'Correo requerido.',
            'legal_id.required' => 'RUC requerido.',
            'legal_id.min' => 'RUC debe tener 13 dígitos.',
            'legal_id.max' => 'RUC debe tener 13 dígitos.',
            'address.required' => 'Dirección requerida.'
        ];

        $request->validate([
            'phone' => 'required',
            'email' => 'required',
            'legal_id' => 'required|min:13|max:13',
            'name' => 'required',
            'address' => 'required'
        ], $messages);

        $client = new Client($request->except(['logo']));

        if ($request->hasFile('logo')) {
            $client->logo = $request->file('logo')->store('logos');
        }
        
        $client->save();

        return redirect()->route('clients.index')->with('success', "El cliente {$client->name} ha sido creado correctamente.");
    }

    public function show(Client $client)
    {
        $services = Service::where('client_id',$client->id)->get();

        return view('clients.show', compact('client','services'));
    }

    public function edit(Client $client)
    {
        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, Client $client)
    {
        $messages = [
            'phone.required' => 'Teléfono requerido.',
            'name.required' => 'Nombre requerido.',
            'email.required' => 'Correo requerido.',
            'legal_id.required' => 'RUC requerido.',
            'legal_id.min' => 'RUC debe tener 13 dígitos.',
            'legal_id.max' => 'RUC debe tener 13 dígitos.',
            'address.required' => 'Dirección requerida.'
        ];
        $request->validate([
            'phone' => 'required',
            'email' => 'required',
            'legal_id' => 'required|min:13|max:13',
            'name' => 'required',
            'address' => 'required'
        ], $messages);
        $client->fill($request->except('logo'));

        if ($request->hasFile('logo')) {
            $client->logo = $request->file('logo')->store('logos');
        }

        $client->save();

        return redirect()->route('clients.index')->with('success', "El cliente {$client->name} ha sido actualizado correctamente.");
    }

    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('clients.index')->with('success', 'El cliente ha sido eliminado correctamente.');
    }
}
