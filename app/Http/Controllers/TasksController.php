<?php

namespace App\Http\Controllers;

use App\Task;
use App\Type;
use App\Status;
use App\Client;
use App\Resource;
use Carbon\Carbon;
use App\StatusType;
use App\Requirement;
use App\Task_Version;
use App\Api\ClickUpApi;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\Object_;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $tasks = Task::when($request->filled('client_id'), function ($query) use ($request) {
            return $query->where('client_id', $request->input('client_id'));
        })->when($request->filled(['from', 'to']), function ($query) use ($request) {
            return $query->whereBetween('due_date', [$request->input('from'), $request->input('to')]);
        })->when($request->filled('type_id'), function ($query) use ($request) {
            return $query->where('type_id', $request->input('type_id'));
        })->when($request->filled('status_id'), function ($query) use ($request) {
            return $query->where('status_id', $request->input('status_id'));
        })->get();
        $types = Type::all();
        $clients = Client::all();
        $statuses = StatusType::where('type', 'tasks')->get()->map(function ($type) {
            return $type->status;
        });
        return view('tasks.index', compact('tasks','types','clients','statuses'));
    }

    public function show(Task $task)
    {
        return view('tasks.show', compact('task'));
    }

    public function calendar(){
        return view('tasks.task_calendar');
    }

    public function store(Request $request, Requirement $requirement)
    {
        $messages = [
            'name.required' => 'Nombre requerido.',
            'due_date.required' => 'Seleccione fecha de entrega.',
            'description.required' => 'Descripción requerida.',
            'resource_id.required' => 'Seleccione recurso.',
            'type_id.required' => 'Seleccione tipo.'
        ];
        $request->validate([
            'name' => 'required',
            'due_date' => 'required',
            'description' => 'required',
            'resource_id' => 'required',
            'type_id' => 'required'
        ], $messages);

        $task = new Task($request->except(['description', '1']));
        $task->status_id = 1;
        $task->client_id = $requirement->client_id;
        $task->requirement_id = $requirement->id;
        $task->save();

        $task_version = new Task_Version($request->only(['description']));
        $task_version->task_id = $task->id;
        $task_version->version = 1;
        $task_version->save();

        return redirect()->route('requirements.show', [$requirement])->with('success', 'La tarea ha sido creada exitosamente.');
    }

    public function edit(Task $task)
    {
        $resources = Resource::all();
        $types = Type::all();
        $statuses = StatusType::where('type', 'tasks')->get()->map(function ($type) {
            return $type->status;
        });

        return view('tasks.edit', compact('task', 'types', 'statuses', 'resources'));
    }

    public function update(Request $request, Task $task)
    {
        $messages = [
            'name.required' => 'Nombre requerido.',
            'status_id.required' => 'Seleccione un estado',
            'resource_id.required' => 'Seleccione un recurso',
            'due_date.required' => 'Seleccione fecha de entrega.',
            'description.required' => 'Descripción requerida.',
            'resource_id.required' => 'Seleccione recurso.',
            'type_id.required' => 'Seleccione tipo.'
        ];
        $request->validate([
            'name' => 'required',
            'due_date' => 'required',
            'resource_id' => 'required',
            'status_id' => 'required',
            'description' => 'required',
            'resource_id' => 'required',
            'type_id' => 'required'
        ], $messages);

        if ($task->lastVersion()->description != $request->input('description')) {
            $new_version = new Task_Version($request->only(['description','files']));
            $new_version->version = $task->lastVersion()->version + 1;
            $new_version->task_id = $task->id;
            $new_version->save();
        }

        $task->update($request->except(['description', 'link','files']));

        if ($request->input('link') === 'requirement') {
            $requirement = Requirement::find($task->requirement_id);
            return redirect()->route('requirements.show', [$requirement])->with('success', 'La tarea ha sido editada exitosamente.');
        }

        return redirect()->route('tasks.index')->with('success', 'La tarea ha sido editada exitosamente.');
    }

    public function destroy(Task $task)
    {
        $task->delete();
        return redirect()->route('tasks.index')->with('success', 'La tarea ha sido eliminada correctamente.');
    }

    public function Project_Task(Task $task)
    {
        $clickup=new ClickUpApi();
        $projects=$clickup->getProjects($task);
        
        return view('tasks.clickup',compact('task','projects'));
    }

    public function Project(Request $request, Task $task)
    {
        $messages=[
            'project_id.required'=>'Seleccione el proyecto.',
            'delay_hour.required'=>'Seleccione la fecha y hora.'
        ];

        $request->validate([
            'project_id' => 'required',
            'delay_hour' => 'required'
        ],$messages);


        $task->delay_hour=$request->delay_hour;
        $task->update();
        $clickup=new ClickUpApi();
        $projects=$clickup->getProjects($task);
        $c=0;
        $lists=null;
        while ($c<sizeof($projects)) {
            if($request->project_id==$projects[$c]['id']){  
                $lists=$projects[$c]['lists'];
                break;
            }
            $c=$c+1;
        }

        $clickup_users=$clickup->getUsers();
        $tmp=$clickup->getSpaces();
        reset($tmp);
        $c1=0;
        $users=null;
        while ($c1<sizeof($tmp)) {
            if($task->resource->space_id==$tmp[$c1]['id']){
                if(array_key_exists('members',$tmp[$c1])){
                    $users=$tmp[$c1]['members'];
                    break;
                }
            }
            $c1=$c1+1;
        }

        if($users==null)
        {
            $users=$clickup_users;
        }
        return view('tasks.list', compact('users', 'task', 'lists'));

    }

    public function postTask(Request $request, Task $task)
    {
        $messages = [
            'list_id.required' => 'Seleccione el tipo.',
            'user_id.required' => 'Seleccione un usuario.'
        ];
        $request->validate([
            'list_id' => 'required',
            'user_id' => 'required'
        ],$messages);


        $clickup=new ClickUpApi();
        $task->clickup_id=$clickup->postTask($request, $task);
        return redirect()->route('tasks.index')->with('success','La tarea ha sido subida con éxito');
    }
    
}
