<?php

namespace App\Http\Controllers;
use App\Type;
use App\Quote;
use App\Resource;
use App\Quotes_Detail;
use Illuminate\Http\Request;

class Quotes_DetailsController extends Controller
{
    public function store(Request $request, Quote $quote)
    {
        $messages=[
            'name.required'=>'Nombre requerido.',
            'type_id.required'=>'Seleccione tipo',
            'description.required'=>'Descripción requerida.',
            'resource_id.required'=>'Seleccione un recurso.',
            'hours.required'=>'Hora requerida.',
            'gain_percent.required'=>'Porcentaje requerido.',
            'dealer_id.required'=>'Seleccione proveerdor',
            'cost.required'=>'Ingrese precio del proveedor.'
        ];

        if ($request->type === 'intern') {
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'resource_id' => 'required',
                'hours' => 'required',
                'type_id' => 'required',
                'gain_percent' => 'required'
            ], $messages);
        } elseif ($request->type === 'extern') {
            $request->validate([
                'name' => 'required',
                'dealer_id' => 'required',
                'description' => 'required',
                'cost' => 'required',
                'gain_percent' => 'required'
            ], $messages);
        }

        $quote_detail = new Quotes_Detail($request->all());
        $type_exter = Type::where('name','Externo')->first();

        if($request->type === 'intern'){
            $quote_detail->cost = Resource::find($request->resource_id)->first()->price * $request->hours;
            $quote_detail->gain = ($quote_detail->cost * $request->gain_percent) / 100;
            $quote_detail->total = $quote_detail->cost + $quote_detail->gain;
        }
        if($request->type === 'extern'){
            $quote_detail->type_id = $type_exter->id;
            $quote_detail->gain = ($quote_detail->cost * $request->gain_percent) / 100;
            $quote_detail->total = $quote_detail->cost + $quote_detail->gain;
        }
        $quote_detail->version=1;
        $quote_detail->group=uniqid();
        $quote_detail->quote_id=$quote->id;
        $quote_detail->save();

        return redirect()->route('quotes.edit', [$quote])->with('success','La cotización ha sido modificada exitosamente.');
    }

    public function update(Request $request, Quotes_Detail $quote_detail)
    {
        $messages = [
            'name.required' => 'Nombre requerido.',
            'name.required' => 'Nombre requerido.',
            'description.required' => 'Descripción requerida.',
            'resource_id.required' => 'Seleccione un recurso.',
            'hours.required' => 'Hora requerida.',
            'gain_percent.required' => 'Porcentaje requerido.',
            'dealer_id.required' => 'Seleccione proveerdor',
            'cost.required' => 'Ingrese precio del proveedor.'
        ];

        if ($request->type === 'intern') {
            $request->validate([
                'name' => 'required',
                'description' => 'required',
                'resource_id' => 'required',
                'type_id' => 'required',
                'hours' => 'required',
                'gain_percent' => 'required'
            ], $messages);
        } elseif($request->type === 'extern') {
            $request->validate([
                'description' => 'required',
                'dealer_id' => 'required',
                'cost' => 'required',
                'type_id' => 'required',
                'gain_percent' => 'required'
            ], $messages);
        }
        
        $value = $quote_detail->Comparator($request->except(['_method', '_token', 'type']));
        
        // If true is not equal
        if ($value) {
            $new_detail = new Quotes_Detail($request->all());
            if ($quote_detail->type === 'intern') {
                $new_detail->cost = Resource::find($request->resource_id)->first()->price * $new_detail->hours;
                $new_detail->gain = ($new_detail->cost * $request->gain_percent) / 100;
                $new_detail->total = $new_detail->cost + $new_detail->gain;
            } elseif ($quote_detail->type === 'extern') {
                $new_detail->gain = ($new_detail->cost * $new_detail->gain_percent) / 100;
                $new_detail->total = $new_detail->cost + $quote_detail->gain;
            }
            $new_detail->quote_id = $quote_detail->quote_id;
            $new_detail->group = $quote_detail->group;
            $new_detail->version = $quote_detail->version + 1;
            $new_detail->save();
        }

        return redirect()->route('quotes.edit', $quote_detail->quote_id)->with('success', 'La cotización ha sido modificada exitosamente.');
    }

    public function destroy(Quotes_Detail $quote_detail)
    {
        $quote = Quote::find($quote_detail->quote_id)->first();
        $quote_detail->delete();
        return redirect()->route('quotes.edit', compact('quote'))->with('success', 'El detalle ha sido eliminado.');
    }
}
