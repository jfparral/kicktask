<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use App\Api\ClickUpApi;

class ResourcesController extends Controller
{
    public function index()
    {
        $resources=Resource::all();
        return view('resources.index', compact('resources'));
    }

    public function create()
    {
        $clickup= new ClickUpApi();
        $spaces=$clickup->getSpaces();
        return view('resources.create',compact('spaces'));
    }

    public function store(Request $request)
    {
        $messages=[
            'name.required'=>'Nombre requerido.',
            'abbr.required'=>'Abreviación requerida.',
            'employees.required' => 'Ingrese número de empleados de este recurso.',
            'price.required' => 'Precio por hora requerido.',
            'space_id.required' => 'ID de espacio en clickup requerido.'
        ];
        $request->validate([
            'name'=>'required',
            'abbr'=>'required',
            'employees' => 'required',
            'price' => 'required',
            'space_id' => 'sometimes|required'
        ], $messages);
        $resource=new Resource($request->all());
        $resource->save();
        return redirect()->route('resources.index')->with('success', 'El recurso ha sido creado exitosamente');
    }

    /* public function show(Resource $resource)
    {
        return view('resources.show', compact('resource'));
    } */

    public function edit(Resource $resource)
    {
        $clickup= new ClickUpApi();
        $spaces=$clickup->getSpaces();
        return view('resources.edit', compact('resource','spaces'));
    }

    public function update(Request $request, Resource $resource)
    {
        $messages=[
            'name.required'=>'Nombre requerido.',
            'abbr.required'=>'Abreviación requerida.',
            'employees.required' => 'Ingrese número de empleados de este recurso.',
            'price.required' => 'Precio por hora requerido.',
            'space_id.required' => 'Seleccione el Espacio en ClickUp'
        ];
        $request->validate([
            'name'=>'required',
            'abbr'=>'required',
            'employees' => 'required',
            'price' => 'required',
            'space_id' => 'sometimes|required'
        ], $messages);
        $resource->update($request->all());

        return redirect()->route('resources.index')->with('success', 'El recurso ha sido editado exitosamente.');
    }

    public function destroy(Resource $resource)
    {
        $resource->delete();
        return redirect()->route('resources.index')->with('success', 'El recurso ha sido eliminado exitosamente.');
    }
}
