<?php

namespace App;
use App\Client;
use App\User; 
use App\Status;
use App\Source;
use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    protected $guarded = [];
    protected $with = ['client', 'contact', 'status','source'];

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function contact()
    {
        return $this->belongsTo('App\User', 'contact_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    public function source()
    {
        return $this->belongsTo('App\Source','source_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task','requirement_id');
    }

}
