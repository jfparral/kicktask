<?php

namespace App\Console;
use DB;
use App\Task;
use App\Quote;
use App\Status;
use App\Requirement;
use App\Api\ClickUpApi;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->call(function(){
             $clickup= new ClickUpApi();
             $tmp=$clickup->getTasks();
             $tasks=$tmp['tasks'];
             $closed=Statuses::where('name','Cerrada')->first();
             foreach($tasks as $task)
             {
                $task_kicktask=Task::where('space_id',$task['id'])->first();
                if($task['statuses']=='Closed'){
                    $task_kicktask->statuses_id=$closed->id;
                    $task_kicktask->update();
                }
             }
             return ;
         })
         ->dailyAt('00:00')
         ->after(function(){
             $requirements=Requirement::all();
             $closed=Statuses::where('name','Cerrada')->first();
             foreach($requirements as $requirement){
                $tasks=Tasks::where('requirement_id',$requirement->id)->get();
                $count=count($tasks);
                $c=0;
                foreach($tasks as $task){
                    if($tasks->status_id==$closed->id){
                        $c+=1;
                    }
                }
                if($count==$c){
                    $requirement->status_id=$closed->id;
                    //send email
                }
             }
            return ;
         });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
