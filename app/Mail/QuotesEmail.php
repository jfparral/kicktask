<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Quote;
use App\Requirement;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuotesEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    public $quote;
    public $pathtofile;
    public $quoteview;
    public $fecha;
    public $company;
    public $exp_date;
    public $token;

    public function __construct(Quote $quote)
    {
        $quote->email_token = uniqid();
        $quote->update();
        $tmp = new Carbon(Requirement::find($quote->requirement_id)->first()->created_at);
        $exp = new Carbon(Requirement::find($quote->requirement_id)->first()->created_at);
        $this->fecha = $tmp->format('d-m-Y');
        $this->exp_date=$exp->format('d-m-Y');
        $this->quote = $quote;
        $this->pathtofile = '/home/jonathan/Escritorio/kicktask/storage/app/public/img/banners_mails_V1.jpg';
        $this->quoteview = 'http://127.0.0.1:8000/view/';
        $this->company = 'KickAds';
        $this->token = $quote->email_token;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this
            ->view('email.mail')
            ->with([
                'quote' => $this->quote,
                'fecha' => $this->fecha,
                'pathtofile' => $this->pathtofile,
                'quoteview' => $this->quoteview,
                'company' => $this->company,
                'exp_date' => $this->exp_date,
                'token' => $this->token
            ]);
    }
}
