<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Quote;
use App\Requirement;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefuseMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $quote;
    public $description;
    public $pathtofile;
    public $quoteview;
    public $fecha;
    public $company;
    public $exp_date;
    public $user;

    public function __construct(Quote $quote, Request $request)
    {
        $tmp = new Carbon(Requirement::find($quote->requirement_id)->first()->created_at);
        $exp = new Carbon(Requirement::find($quote->requirement_id)->first()->created_at);
        $this->fecha = $tmp->format('d-m-Y');
        $this->exp_date = $exp->format('d-m-Y');
        $this->description = $request->input('description');
        $this->quote = $quote;
        $this->pathtofile = '/home/jonathan/Escritorio/6/images/banners_mails_V1.jpg';
        $this->quoteview = 'http://127.0.0.1:8000/view/';
        $this->company = 'KickAds';
        $this->user = $quote->user->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('email.refuseMail')
            ->with([
                'quote' => $this->quote,
                'description' => $this->description,
                'fecha' => $this->fecha,
                'pathtofile' => $this->pathtofile,
                'quoteview' => $this->quoteview,
                'company' => $this->company,
                'exp_date' => $this->exp_date,
            ]);
    }
}
