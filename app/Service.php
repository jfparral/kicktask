<?php

namespace App;

use App\Resource;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];
    protected $with = ['resource', 'servicesdetails', 'client'];

    public function resource()
    {
        return $this->belongsTo('App\Resource','resource_id');
    }

    public function servicesdetails()
    {
        return $this->hasMany('App\Services_detail','service_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }
}
