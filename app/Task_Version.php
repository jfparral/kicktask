<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_Version extends Model
{
    protected $guarded=[];
    protected $table="task_versions";

    public function task()
    {
        return $this->belongsTo('App\Task','task_id');
    }
}
