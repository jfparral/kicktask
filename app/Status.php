<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function requirements()
    {
        return $this->hasMany('App\Status', 'status_id');
    }

}
