<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        KickTask
    </title>
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
    <link href="{{ asset('dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/default/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dist/default/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div id="app">
        @yield('content')
    </div>
    
    <!--begin::Base Scripts -->
    {{--  <script src="./assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="./assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>  --}}
    <!--end::Base Scripts -->
    
    <!--begin::Page Snippets -->
    {{--  <script src="./assets/snippets/pages/user/login.js" type="text/javascript"></script>  --}}
    <!--end::Page Snippets -->
</body>
</html>