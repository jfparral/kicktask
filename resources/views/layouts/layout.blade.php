<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			KickAds | Tareas
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
		<link href="{{ asset('dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('dist/default/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('dist/default/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
	</head>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- Header -->
            @include('partials._header')	
			<!-- Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- Aside -->
				@include('partials._aside')
				<!-- Content -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					@yield('content')
				</div>
			</div>
			<!-- Footer -->
			@include('partials._footer')
		</div>	

    	<!-- Scripts -->
		<script src="{{ asset('dist/default/assets/vendors/base/vendors.bundle.js') }}"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<script src="{{ asset('dist/default/assets/demo/default/base/scripts.bundle.js') }}"></script>
		{{-- <script src="{{ asset('dist/default/assets/app/js/dashboard.js') }}"></script> --}}
		@stack('scripts')
		{{-- Toast for success --}}
		@if(session()->has('success'))
			<script>
				toastr.success('{{ session()->get('success') }}', 'Operación existosa', {
					closeButton: true				
				})
			</script>
		@endif
	</body>
</html>
