@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Calendario de Tareas'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.calendar')}}" class="m-nav__link">
        <span class="m-nav__link-text">Calendario de Tareas</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-portlet" id="m_portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon">
                                        <i class="flaticon-calendar"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        List View
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>Add Event</span>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div id="m_calendar" class="fc fc-unthemed fc-ltr">
                                <div class="fc-toolbar fc-header-toolbar">
                                    <div class="fc-left">
                                        <div class="fc-button-group">
                                            <button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left">
                                                <span class="fc-icon fc-icon-left-single-arrow"></span>
                                            </button>
                                            <button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right">
                                                <span class="fc-icon fc-icon-right-single-arrow"></span>
                                            </button>
                                        </div>
                                        <button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled"
                                            disabled="">today</button>
                                    </div>
                                    <div class="fc-right">
                                        <div class="fc-button-group">
                                            <button type="button" class="fc-month-button fc-button fc-state-default fc-corner-left">month</button>
                                            <button type="button" class="fc-agendaDay-button fc-button fc-state-default fc-state-active">day</button>
                                            <button type="button" class="fc-listWeek-button fc-button fc-state-default fc-corner-right">list</button>
                                        </div>
                                    </div>
                                    <div class="fc-center">
                                        <h2>April 20, 2018</h2>
                                    </div>
                                    <div class="fc-clear"></div>
                                </div>
                                <div class="fc-view-container" style="">
                                    <div class="fc-view fc-agendaDay-view fc-agenda-view" style="">
                                        <table class="">
                                            <thead class="fc-head">
                                                <tr>
                                                    <td class="fc-head-container fc-widget-header">
                                                        <div class="fc-row fc-widget-header" style="border-right-width: 1px; margin-right: 14px;">
                                                            <table class="">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="fc-axis fc-widget-header" style="width:45px"></th>
                                                                        <th class="fc-day-header fc-widget-header fc-fri fc-today" data-date="2018-04-20">
                                                                            <span>Friday</span>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody class="fc-body">
                                                <tr>
                                                    <td class="fc-widget-content">
                                                        <div class="fc-day-grid fc-unselectable">
                                                            <div class="fc-row fc-week fc-widget-content" style="border-right-width: 1px; margin-right: 14px;">
                                                                <div class="fc-bg">
                                                                    <table class="">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="fc-axis fc-widget-content" style="width:45px">
                                                                                    <span>all-day</span>
                                                                                </td>
                                                                                <td class="fc-day fc-widget-content fc-fri fc-today " data-date="2018-04-20"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="fc-content-skeleton">
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="fc-axis" style="width:45px"></td>
                                                                                <td class="fc-event-container">
                                                                                    <a class="fc-day-grid-event fc-h-event fc-event fc-not-start fc-end m-fc-event--accent fc-draggable fc-resizable" data-original-title=""
                                                                                        title="">
                                                                                        <div class="fc-content">
                                                                                            <span class="fc-title">Conference</span>
                                                                                        </div>
                                                                                        <div class="fc-resizer fc-end-resizer"></div>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr class="fc-divider fc-widget-header">
                                                        <div class="fc-scroller fc-time-grid-container" style="overflow-x: hidden; overflow-y: scroll; height: 740px;">
                                                            <div class="fc-time-grid fc-unselectable">
                                                                <div class="fc-bg">
                                                                    <table class="">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="fc-axis fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-day fc-widget-content fc-fri fc-today " data-date="2018-04-20"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="fc-slats">
                                                                    <table class="">
                                                                        <tbody>
                                                                            <tr data-time="00:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>12am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="00:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="01:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>1am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="01:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="02:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>2am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="02:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="03:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>3am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="03:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="04:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>4am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="04:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="05:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>5am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="05:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="06:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>6am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="06:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="07:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>7am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="07:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="08:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>8am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="08:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="09:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>9am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="09:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="10:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>10am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="10:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="11:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>11am</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="11:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="12:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>12pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="12:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="13:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>1pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="13:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="14:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>2pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="14:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="15:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>3pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="15:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="16:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>4pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="16:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="17:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>5pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="17:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="18:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>6pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="18:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="19:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>7pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="19:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="20:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>8pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="20:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="21:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>9pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="21:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="22:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>10pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="22:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="23:00:00">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px">
                                                                                    <span>11pm</span>
                                                                                </td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                            <tr data-time="23:30:00" class="fc-minor">
                                                                                <td class="fc-axis fc-time fc-widget-content" style="width:45px"></td>
                                                                                <td class="fc-widget-content"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <hr class="fc-divider fc-widget-header" style="display:none">
                                                                <div class="fc-content-skeleton">
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="fc-axis" style="width:45px"></td>
                                                                                <td>
                                                                                    <div class="fc-content-col">
                                                                                        <div class="fc-event-container fc-helper-container"></div>
                                                                                        <div class="fc-event-container">
                                                                                            <a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end fc-draggable fc-resizable" style="top: 755px; bottom: -899px; z-index: 1; left: 0%; right: 0%; margin-right: 20px;">
                                                                                                <div class="fc-content">
                                                                                                    <div class="fc-time" data-start="10:30" data-full="10:30 AM - 12:30 PM">
                                                                                                        <span>10:30 - 12:30</span>
                                                                                                    </div>
                                                                                                    <div class="fc-title">Meeting
                                                                                                        <div class="fc-description">Lorem ipsum dolor
                                                                                                            eiu idunt ut
                                                                                                            labore
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="fc-bg"></div>
                                                                                                <div class="fc-resizer fc-end-resizer"></div>
                                                                                            </a>
                                                                                            <a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end m-fc-event--info fc-draggable fc-resizable" style="top: 863px; bottom: -1007px; z-index: 2; left: 50%; right: 0%;">
                                                                                                <div class="fc-content">
                                                                                                    <div class="fc-time" data-start="12:00" data-full="12:00 PM">
                                                                                                        <span>12:00</span>
                                                                                                    </div>
                                                                                                    <div class="fc-title">Lunch
                                                                                                        <div class="fc-description">Lorem ipsum dolor
                                                                                                            sit amet, ut
                                                                                                            labore
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="fc-bg"></div>
                                                                                                <div class="fc-resizer fc-end-resizer"></div>
                                                                                            </a>
                                                                                            <a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end m-fc-event--warning fc-draggable fc-resizable" style="top: 1043px; bottom: -1187px; z-index: 1; left: 0%; right: 0%;">
                                                                                                <div class="fc-content">
                                                                                                    <div class="fc-time" data-start="2:30" data-full="2:30 PM">
                                                                                                        <span>2:30</span>
                                                                                                    </div>
                                                                                                    <div class="fc-title">Meeting
                                                                                                        <div class="fc-description">Lorem ipsum conse
                                                                                                            ctetur adipi
                                                                                                            scing
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="fc-bg"></div>
                                                                                                <div class="fc-resizer fc-end-resizer"></div>
                                                                                            </a>
                                                                                            <a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end m-fc-event--metal fc-draggable fc-resizable" style="top: 1259px; bottom: -1403px; z-index: 1; left: 0%; right: 0%;">
                                                                                                <div class="fc-content">
                                                                                                    <div class="fc-time" data-start="5:30" data-full="5:30 PM">
                                                                                                        <span>5:30</span>
                                                                                                    </div>
                                                                                                    <div class="fc-title">Happy Hour
                                                                                                        <div class="fc-description">Lorem ipsum dolor
                                                                                                            sit amet, conse
                                                                                                            ctetur
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="fc-bg"></div>
                                                                                                <div class="fc-resizer fc-end-resizer"></div>
                                                                                            </a>
                                                                                            <a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end fc-draggable fc-resizable" style="top: 1439px; bottom: -1583px; z-index: 1; left: 0%; right: 0%;">
                                                                                                <div class="fc-content">
                                                                                                    <div class="fc-time" data-start="8:00" data-full="8:00 PM">
                                                                                                        <span>8:00</span>
                                                                                                    </div>
                                                                                                    <div class="fc-title">Dinner
                                                                                                        <div class="fc-description">Lorem ipsum dolor
                                                                                                            sit ctetur adipi
                                                                                                            scing
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="fc-bg"></div>
                                                                                                <div class="fc-resizer fc-end-resizer"></div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="fc-highlight-container"></div>
                                                                                        <div class="fc-bgevent-container"></div>
                                                                                        <div class="fc-business-container"></div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('scripts')
<script src="./assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="./assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="./assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="./assets/demo/default/custom/components/calendar/list-view.js" type="text/javascript"></script>
@endpush