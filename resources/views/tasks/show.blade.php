@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Tareas'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.show',[$task])}}" class="m-nav__link">
        <span class="m-nav__link-text">Ver</span>
    </a>
</li>
@slot('options')
<a href="{{ route('tasks.index') }}" class="btn btn-metal">Regresar</a>
@endslot @endbreadcrumbs
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div class="m-portlet__body mb-0">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Nombre:</h3>
                                        <span class="m--font-boldest">{{$task->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <div class="m-section__heading">
                                            <h3>
                                                Descripción:
                                                <span class="m-badge m-badge--info m-badge--wide">Versión {{$task->lastVersion()->version}}</span>
                                            </h3>
                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#versiontask{{$task->id}}">
                                                <i class="m-menu__link-icon flaticon-visible"></i>
                                                Ver Versiones
                                            </button>
                                        </div>
                                        <span class="m--font-boldest">{!!$task->lastVersion()->description!!}</span>
                                        

                                        <!-- Modal Show-->
                                        <div class="modal fade show" role="document" id="versiontask{{$task->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-lg m--align-left">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Descripciones</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @foreach($task->versions as $versionTask)
                                                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                <div class="m-stack__item">
                                                                    <div class="m-stack__demo-item">
                                                                        <h3 class="m-section__heading"><span class="m-badge m-badge--info m-badge--wide">Versión {{$versionTask->version}}</span></h3>
                                                                        <span class="m--font-boldest">{!!$versionTask->description!!}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Fecha de Entrega:</h3>
                                        <span class="m--font-boldest">{{$task->due_date}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Área de Recurso:</h3>
                                        <span class="m--font-boldest">{{$task->resource->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Estado:</h3>
                                        <span class="m--font-boldest">{{$task->status->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Tipo:</h3>
                                        <span class="m--font-boldest">{{$task->type->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection