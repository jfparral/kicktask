@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Subir Tarea a ClickUp'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.Project_Task',[$task])}}" class="m-nav__link">
        <span class="m-nav__link-text">Subir Tarea a ClickUp</span>
    </a>
</li>
@slot('options')
@endslot @endbreadcrumbs
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section__content">
                        <div class="m-portlet m-portlet--tab">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text"> Ingrese Detalles para ClickUp</h3>
                                    </div>
                                </div>
                            </div>
                            <form class="m-form m-form--fit m-form--label-align-right m-form--state" action="{{route('tasks.Project',[$task])}}" method="post">
                                @csrf
                                <div class="form-group m-form__group @if($errors->has('project_id')) has-danger @endif">
                                    <label for="space_id">Proyecto:</label>
                                    <select class="form-control selectpicker col-lg-4" name="project_id" id="project_id">
                                        <option value="">ClickUp Space</option>
                                        @foreach($projects as $project)
                                        <option value="{{$project['id']}}">{{$project['name']}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('project_id'))
                                    <div class="form-control-feedback">{{ $errors->first('project_id') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group @if($errors->has('delay_hour')) has-danger @endif">
                                    <label for="delay_hour">Horas Estimadas de Trabajo:</label>
                                    <input type='datetime-local' name="delay_hour" class="form-control col-lg-5" id='datetimepicker4' placeholder="Seleccione hora"/>
                                    @if($errors->has('delay_hour'))
                                    <div class="form-control-feedback">{{ $errors->first('delay_hour') }}</div>
                                    @endif
                                </div>
                                <div class="m-form__actions">
                                    <a href="{{ route("tasks.index")}}" class="btn btn-metal">Cancelar</a>
                                    <button type="submit"  class="btn btn-success">Siguiente</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('.project_id').select2({
        placeholder: "Seleccionar recurso"
    });
</script>
@endpush