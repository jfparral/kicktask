@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Tareas'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<div class="m-content">
    <div class="m-porlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Ingrese los Detalles de la tarea</h3>
                    </div>
                </div>
            </div>
            <div class="m-porlet__body">
                <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="POST" action="{{ route('tasks.update', [$task]) }}">
                    @csrf @method('PATCH')
                    <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                        <label for="name"> Nombre:</label>
                        <input id="name" type="text" class="form-control col-lg-3" value="{{$task->name}}" name="name"
                            placeholder="Ingrese nombre"> @if($errors->has('name'))
                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('due_date')) has-danger @endif">
                        <label for="date"> Fecha:</label>
                        <input name="due_date" class="form-control date col-lg-3" type="date" value="{{$task->due_date}}" placeholder="Ingrese fecha" readonly
                            id="date"> @if($errors->has('due_date'))
                        <div class="form-control-feedback">{{ $errors->first('due_date') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                        <label for="description">Descripción</label>
                        <div class="col-sm-10">
                            <textarea type="description" class="form-control m-input col-lg-8" id="description" value="" placeholder="Ingrese descripción" name="description"
                                cols="30" rows="5">{{$task->lastVersion()->description}}</textarea>
                            @if($errors->has('description'))
                            <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group @if($errors->has('type_id')) has-danger @endif">
                        <label for="type_id">Tipo:</label>
                        <select class="form-control selectpicker col-sm-5" name="type_id" id="type_id">
                            <option value="">Seleccionar Tipo</option>
                            @foreach($types as $type)
                            <option value="{{$type->id}}" @if($type->id === $task->type_id) selected @endif>{{$type->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('type_id'))
                        <div class="form-control-feedback">{{ $errors->first('type_id') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('status_id')) has-danger @endif">
                        <label for="status_id">Estado:</label>
                        <select class="form-control selectpicker col-sm-5" name="status_id" id="status_id">
                            <option value="">Estado</option>
                            @foreach($statuses as $status)
                            <option value="{{$status->id}}" @if($status->id === $task->status_id) selected @endif>{{$status->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('status_id'))
                        <div class="form-control-feedback">{{ $errors->first('status_id') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                        <label for="resource_id">Recurso:</label>
                        <select class="form-control selectpicker col-lg-5" name="resource_id" id="resource_id">
                            <option value="">Recursos</option>
                            @foreach($resources as $resource)
                            <option value="{{$resource->id}}" @if($resource->id === $task->resource_id) selected @endif>{{$resource->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('resource_id'))
                        <div class="form-control-feedback">{{ $errors->first('resource_id') }}</div>
                        @endif
                    </div>
                    <input name='link' type="text" hidden="true" value="">
                    <div class="m-form__actions">
                        <a href="{{ route('tasks.index')}}" class="btn btn-metal">Cancelar</a>
                        <button type="submit" class="btn btn-success">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection @push('scripts')
<script>
    $('.date').datepicker({
        orientation: "bottom left",
        autoclose: true,
        format: "yyyy-mm-dd",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    $('.resource_id').select2({
        placeholder: "Seleccionar recurso"
    });
    $('.status_id').select2({
        placeholder: "Seleccionar estado"
    });
    $('.type_id').select2({
        placeholder: "Seleccionar tipo"
    });
    $('.summernote').summernote({
        height: 140
    });
</script>
@endpush