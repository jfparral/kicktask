@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Tareas'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('tasks.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section__content">
                        <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="GET" action="{{ route('tasks.index') }}">
                            <div class="row d-flex p-4">
                                <div class="col-lg-4">
                                    <label for="client_id">Cliente:</label>
                                    <select class="form-control selectpicker" name="client_id" id="client_id">
                                        <option value=""> Seleccionar cliente</option>
                                        @foreach($clients as $client)
                                        <option value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="status_id">Estado:</label>
                                    <select class="form-control selectpicker" name="status_id" id="status_id">
                                        <option value=""> Seleccionar estado</option>
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="type_id">Tipo:</label>
                                    <select class="form-control selectpicker" name="type_id" id="type_id">
                                        <option value=""> Seleccionar tipo</option>
                                        @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2 align-items-center">
                                    <label for="from">Desde:</label>
                                    <input type="text" class="form-control date" name="from" id="from" readonly placeholder="Fecha desde" />
                                </div>
                                <div class="col-lg-2 align-items-center">
                                    <label for="to">Hasta:</label>
                                    <input type="text" class="form-control date" name="to" id="to" readonly placeholder="Fecha hasta" />
                                </div>
                                <div class="col-lg-1 d-flex align-items-end">
                                    <button class="btn btn-success">Filtrar</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-bordered m-table m-table--head-bg-brand">
                            <thead>
                                <tr>
                                    <th class="m--img-centered">#</th>
                                    <th class="m--img-centered">Nombre</th>
                                    <th class="m--img-centered">Estado</th>
                                    <th class="m--img-centered">Versión</th>
                                    <th class="m--img-centered">Recurso</th>
                                    <th class="m--img-centered">Tipo</th>
                                    <th class="m--img-centered">Fecha de Entrega</th>
                                    <th class="m--img-centered">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                <tr>
                                    <td class="m--align-center"> {{$loop->iteration}} </td>
                                    <td class="m--align-center">{{$task->name}}</td>
                                    <td class="m--align-center">
                                        <span class="m-badge m-badge--primary m-badge--wide">{{$task->status->name}}</span>
                                    </td>
                                    <td class="m--align-center">
                                        <span class="m-badge m-badge--warning m-badge--wide">{{$task->lastversion()->version}}</span>
                                    </td>
                                    <td class="m--align-center">{{$task->resource->name}}</td>
                                    <td class="m--align-center">{{$task->type->name}}</td>
                                    <td class="m--align-center">{{$task->due_date}}</td>
                                    <td class="m--align-center">
                                        <a href="{{ route('tasks.show', [$task])}}" class="btn btn-success btn-sm">
                                            <i class="m-menu__link-icon flaticon-visible"></i>
                                        </a>
                                        <a href="{{ route('tasks.edit', [$task])}}" class="btn btn-info btn-sm">
                                            <i class="m-menu__link-icon flaticon-edit"></i>
                                        </a>
                                        @if($task->status->name!='Enviada' && $task->type->name!='Externo')
                                        <a href="{{route('tasks.Project_Task',[$task])}}" class="btn btn-warning btn-sm">
                                            <i class="m-menu__link-icon flaticon-paper-plane"></i>
                                        </a>
                                        @endif

                                        <!-- Button trigger modal destroy-->
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$task->id}}">
                                            <i class="m-menu__link-icon flaticon-circle"></i>
                                        </button>

                                        <!-- Modal Destroy-->
                                        <div class="modal fade" id="eliminarModal{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Tarea {{ $task->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Está seguro que desea eliminar esta Tarea?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                       <form action="{{ route('tasks.destroy', [$task])}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-primary" type="submit">
                                                                Eliminar
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('.date').datepicker({
            orientation: "bottom left",
            autoclose: true,
            format: "yyyy-mm-dd",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#client_id').select2({
            placeholder: "Seleccionar cliente"
        });
        $('#status_id').select2({
            placeholder: "Seleccionar estado"
        });
        $('#type_id').select2({
            placeholder: "Seleccionar tipo"
        });
</script>
<script src="./assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="./assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="./assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="./assets/demo/default/custom/components/calendar/list-view.js" type="text/javascript"></script>
@endpush