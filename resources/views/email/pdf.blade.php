<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>COTIZACIÓN</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #0087C3;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 21cm;
            height: auto;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-size: 14px;
            font-family: 'Times New Roman', Times, serif;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            float: left;
            margin-top: 8px;
        }

        #logo img {
            height: 50px;
        }

        #company {
            float: right;
            text-align: right;
        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 10px;
            border-left: 6px solid #ff0000;
            float: left;
        }

        #client .to {
            color: #272936;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            float: right;
            text-align: right;
        }

        #invoice h1 {
            color: #272936;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        .table_client {
            align-content: center;
            width: 90%;
            border-collapse: collapse;
            border-spacing: 0;
        }

        table {
            align-content: center;
            width: 90%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 0;
        }

        table th,
        table td {
            padding: 10px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: center;
        }

        .description {
            text-align: left;
        }

        .resource {
            text-align: center;
            border-left: 1px solid #FFFFFF;
        }

        table td h3 {
            color: #272936;
            font-size: 1.2em;
            font-weight: bold;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1.1em;
            background: #272936;
            border-left: 1px solid #FFFFFF;
        }

        table .desc {
            text-align: justify;
            background: #DDDDDD;
        }

        table .unit {
            color: #555555;
            font-size: 1.1em;
            background: #d0d0d0;
            border-left: 1px solid #FFFFFF;
        }

        table .qty {
            text-align: justify;
        }

        table .total {
            background: #272936;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 10px 15px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            margin-top: 50px;
            marging-bottom: 25px;
        }

        table tfoot tr:first-child td {
            marging-top: 50px;
            border-top: none;
        }

        table tfoot tr:last-child td {
            color: #282a36;
            font-size: 1.4em;
            font-weight: bold;
        }

        table tfoot tr td:first-child {
            marging-top: 50px;
            border: none;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #ff0000;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>

<body style="align-content: center;">
    <header>
        <table>
            <tbody>
                <tr>
                    <td rowspan="6" style="padding:1px;border-bottom:0px;background:white;text-align:center;">
                        <img src="/home/jonathan/Escritorio/kicktask/storage/app/public/img/logo.jpg" height="70px">
                    </td>
                </tr>
                <tr>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;"></td>
                    <td id="company" class="name" style="padding:1px;border-bottom:0px;background:white;text-align:right;">KICKADS S.A.</td>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;padding-left:0px;"></td>
                </tr>
                <tr>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;"></td>
                    <td id="company" style="padding:1px;border-bottom:0px;background:white;text-align:right;">Kennedy Norte Mz. 504 S.3-4 Edificio Colon 104</td>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;padding-left:0px;"></td>
                </tr>
                <tr>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;"></td>
                    <td id="company" style="padding:1px;border-bottom:0px;background:white;text-align:right;">(602) 519-0450</td>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:center;padding-left:px;"></td>
                </tr>
            </tbody>
        </table>

        <table>
            <tbody>
                <tr style="padding:0px">
                    <td style="padding:0px;border-bottom:0px;background:white;"></td>
                    <td style="padding:0px;border-bottom:0px;background:white;text-align:right;">
                        <h1>COTIZACIÓN #{{$quote->id}}</h1>
                    </td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">COTIZACIÓN A: </td>
                    <td style="padding:0px;border-bottom:0px;background:white;text-align:right;">Fecha de Cotización: {{$quote->updated_at}}</td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">{{$quote->client_name}}</td>
                    <td style="padding:1px;border-bottom:0px;background:white;text-align:right;">Fecha de Expiración: {{$quote->exp_date}}</td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">{{$quote->client_address}}</td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">
                        <a href="{{$quote->client_email}}">{{$quote->client_email}}</a>
                    </td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">SOLICITADO POR: </td>
                    
                </tr>
                <tr style="padding:0px">
                    
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">{{$quote->contact->name}}</td>
                </tr>
                <tr style="padding:0px">
                    <td id="client" style="padding:0px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">
                        <a href="{{$quote->contact->email}}">{{$quote->contact->email}}</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </header>
    <main>



        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="no resource">#</th>
                    <th class="no resource">DETALLE</th>
                    <th class="no resource">PRECIO UNITARIO</th>
                </tr>
            </thead>
            <tbody>
                @foreach($quotes_details_intern as $quote_detail)
                <tr>
                    <td class="unit">{{$loop->iteration}}</td>
                    <td class="detail description">
                        <h3>{{$quote_detail->name}}</h3>{{$quote_detail->description}}</td>
                    <td class="resource unit">${{$quote_detail->total}}</td>
                </tr>
                @endforeach
                @foreach($quotes_details_extern as $quote_detail)
                <tr>
                    <td class="unit">{{$loop->iteration+count($quotes_details_intern)}}</td>
                    <td class="detail description">
                        <h3>{{$quote_detail->name}}</h3>{{$quote_detail->description}}</td>
                    <td class="resource unit">${{$quote_detail->total}}</td>
                </tr>
                @endforeach
                <tr>
                    <td style="padding:10px;border-bottom:10px;background:white;text-align:center;"></td>
                    <td style="padding:10px;border-bottom:10px;background:white;text-align:center;"></td>
                    <td style="padding:10px;border-bottom:10px;background:white;text-align:center;"></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2" style="padding-top=20px">SUBTOTAL</td>
                    <td style="padding-top=20px">${{$subtotal}}</td>
                </tr>
                <tr>
                    <td colspan="2">IVA 12%</td>
                    <td>${{$iva}}</td>
                </tr>
                <tr>
                    <td colspan="2" style="color: #282a36;font-weight:bold;font-size:18px;">TOTAL</td>
                    <td style="color: #282a36;font-weight:bold;font-size:18px;">${{$total}}</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        <table>
            <tbody>
                <tr>
                    <td id="notices" style="padding:1px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">TÉRMINOS LEGALES:</td>
                </tr>
                <tr>
                    <td id="notices" style="padding:1px;border-bottom:0px;background:white;text-align:left; padding-left:15px;">A finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 daysA finance charge of 1.5% will be made on unpaid balances after 30 days.</td>
                </tr>
            </tbody>
        </table>
    </main>
    <footer>
        <table>
            <tr>
                <td style="padding:1px;background:white;text-align:center; padding-left:5px;">KICKTASK 2018</td>
            </tr>
        </table>

    </footer>
</body>

</html>