<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>COTIZACIÓN</title>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<!--
			CSS
			============================================= -->
	<link rel="stylesheet" href="public/../../../css/linearicons.css">
	<link rel="stylesheet" href="public/../../../css/font-awesome.min.css">
	<link rel="stylesheet" href="public/../../../css/bootstrap.css">
	<link rel="stylesheet" href="public/../../../css/magnific-popup.css">
	<link rel="stylesheet" href="public/../../../css/nice-select.css">
	<link rel="stylesheet" href="public/../../../css/animate.min.css">
	<link rel="stylesheet" href="public/../../../css/owl.carousel.css">
	<link rel="stylesheet" href="public/../../../css/main.css">
	<link href="{{ asset('dist/default/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body>

	<header id="header" id="home">
		<div class="container">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="http://www.wekickads.com">
						<img src="../../storage/img/kickads-logo.png" alt="" title="" height="50">
					</a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li class="menu-active">
							<a href="http://www.wekickads.com/">KICKCADS</a>
						</li>
						<li class="menu-active">
							<a href="{{route('email.pdf_download',$quote->id)}}" target="_blank">DESCARGAR PDF</a>
						</li>
					</ul>
				</nav>
				<!-- #nav-menu-container -->
			</div>
		</div>
	</header>
	<!-- #header -->


	<!-- start banner Area -->
	<section class="banner-area" id="home">
		<div class="container" style="padding-top: 200px; padding-bottom: 200px;">
			<div class="row d-flex align-items-center">
				<div class="container">
					<div id="details" class="clearfix">
						<div id="client">
							<div class="to">COTIZACIÓN A:</div>
							<h2 class="name">{{$quote->client->name}}</h2>
							<div class="address">{{$quote->client->address}}</div>
							<div class="email">
								<a href="mailto:john@example.com">{{$quote->client->email}}</a>
							</div>
							<div class="to">SOLICITADO POR:</div>
							<h2 class="name">{{$quote->contact->name}}</h2>
							<div class="email">
								<a href="mailto:john@example.com">{{$quote->contact->email}}</a>
							</div>
						</div>
						<div id="invoice">
							<h1>COTIZACIÓN #{{$quote->id}}</h1>
							<div class="date">Fecha de Cotización: {{$quote->updated_at}}</div>
							<div class="date">Fecha de Expiración: {{$quote->exp_date}}</div>
						</div>
					</div>
					<table border="0" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th class="unit"></th>
								<th class="unit">DETALLE</th>
								<th class="unit">PRECIO UNITARIO</th>
							</tr>
						</thead>
						<tbody>
							@foreach($quotes_details_intern as $quote_detail)
								<tr>
									<td class="no">{{$loop->iteration}}</td>
									<td class="detail">
										<h3>{{$quote_detail->name}}</h3>{{$quote_detail->description}}</td>
									<td class="total">${{$quote_detail->total}}</td>
								</tr>
							@endforeach
							@foreach($quotes_details_extern as $quote_detail)
								<tr>
									<td class="no">{{$loop->iteration+count($quotes_details_intern)}}</td>
									<td class="detail">
										<h3>{{$quote_detail->name}}</h3>{{$quote_detail->description}}</td>
									<td class="total">${{$quote_detail->total}}</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="1"></td>
								<td colspan="1" style="font-weight:900;">SUBTOTAL</td>
								<td>${{$subtotal}}</td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td colspan="1">IVA 12%</td>
								<td>${{$iva}}</td>
							</tr>
							<tr>
								<td colspan="1"></td>
								<td colspan="1" style="color: #282a36;font-weight:bold;font-size:18px;">TOTAL</td>
								<td style="color: #282a36;font-weight:bold;font-size:18px;background:#efefef;">${{$total}}</td>
							</tr>
							<tr>
								<td colspan="2"></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
					<div id="notices">
						<div class="notice" style="font-weight: bold; color:#868686; text-decoration: underline #868686;">TÉRMINOS LEGALES:</div>
						<div class="notice">{!!$quote->terms!!}</div>
					</div>

					<div style="margin:auto; text-align:center;">
						@if($quote->status->name != 'Aprobada' && $quote->status->name != 'Rechazada')
							<!-- Modal -->
							<button type="button" class="genric-btn primary" data-toggle="modal" data-target="#acceptModal{{$quote->id}}">
								ACEPTAR
							</button>
							<button type="button" class="genric-btn success" data-toggle="modal" data-target="#refuseModal{{$quote->id}}">
								RECHAZAR
							</button>
						@endif


						<!-- Modal Accept-->
						<div class="modal fade" id="acceptModal{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Aceptar Cotización</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											¿Está seguro que desea aceptar esta cotización?
										</div>
										<div class="modal-footer">
											<button type="button" class="genric-btn default" data-dismiss="modal">Cancelar</button>
											<a class="genric-btn primary" href="{{route('email.accept',[$quote->email_token])}}">Aceptar</a>
										</div>
									</form>
								</div>
							</div>
						</div>

						<!-- Modal Refuse-->
						<div class="modal fade" id="refuseModal{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<form class="m-form m-form--fit" method="POST" action="{{route('email.refuse',[$quote->email_token])}}">
                                    @csrf
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Rechazar Cotización</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<label for="text">Ingrese sus observaciones:</label>
											<textarea name="description" id="cescription" cols="30" rows="5" class="form-control"></textarea>
										</div>
										<div class="modal-footer">
											<button type="button" class="genric-btn default" data-dismiss="modal">Cancelar</button>
											<button type="submit" class="genric-btn success">Rechazar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->




	<!-- start footer Area -->
	<footer>
		<div class="conteiner" style="background: #333">
			KICKTASK S.A. 2018
		</div>
	</footer>
	<!-- End footer Area -->

	<script src="public/../../../js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	    crossorigin="anonymous"></script>
	<script src="public/../../../js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="public/../../../js/easing.min.js"></script>
	<script src="public/../../../js/hoverIntent.js"></script>
	<script src="public/../../../js/superfish.min.js"></script>
	<script src="public/../../../js/jquery.ajaxchimp.min.js"></script>
	<script src="public/../../../js/jquery.magnific-popup.min.js"></script>
	<script src="public/../../../js/owl.carousel.min.js"></script>
	<script src="public/../../../js/jquery.sticky.js"></script>
	<script src="public/../../../js/jquery.nice-select.min.js"></script>
	<script src="public/../../../js/parallax.min.js"></script>
	<script src="public/../../../js/mail-script.js"></script>
	<script src="public/../../../js/main.js"></script>
</body>

</html>