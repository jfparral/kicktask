<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>

  </title>
  <!--[if !mso]><!-- -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }
  </style>

  <style type="text/css">
    @media only screen and (max-width:480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
  </style>



  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
      }
    }
  </style>


  <style type="text/css">
  </style>

</head>

<body style="background-color:#eaeced;">


  <div style="background-color:#eaeced;">

    <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">

              <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                  <tbody>
                    <tr>
                      <td style="vertical-align:top;padding:0px;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">

                          <tr>
                            <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">

                              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                <tbody>
                                  <tr>
                                    <td style="width:600px;">

                                      <img alt="header image" height="auto" src="{{$message->embed($pathtofile)}}" style="border:0;display:block;outline:none;text-decoration:none;width:100%;"
                                        width="600">

                                    </td>
                                  </tr>
                                </tbody>
                              </table>

                            </td>
                          </tr>

                        </table>

                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>

            </td>
          </tr>
        </tbody>
      </table>

    </div>

    <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">

      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:0px;padding-bottom:20px;padding-top:10px;text-align:center;vertical-align:top;">

              <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                  <tbody>
                    <tr>
                      <td style="vertical-align:top;padding:0px;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">

                          <tr>
                            <td align="center" style="font-size:0px;padding:30px 25px;word-break:break-word;">
                              <div style="font-family:Helvetica;font-size:20px;line-height:1;text-align:center;color:#252b33!important;">
                                <strong>Hey {{$user}}!</strong>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td align="center" style="font-size:0px;padding:0 25px;word-break:break-word;">
                              <div style="font-family: 'Lato', sans-serif;font-weight: normal;margin-left:30px;margin-right:30px;font-size:16px;line-height:2;text-align:center;color:#7e8890!important;">
                                Estimado cliente {{$quote->client_name}} ha aprobado la cotización N°{{$quote->id}} generada el {{$fecha}}.
                              </div>
                            </td>
                          </tr>

                           <tr>
                            <td align="center" style="font-size:0px;padding:0 25px;word-break:break-word; padding-top:25px;">
                              <div style="font-family: 'Lato', sans-serif;font-weight: normal;margin-left:30px;margin-right:30px;font-size:16px;line-height:2;text-align:center;color:#7e8890!important;">
                                NOTA: Esta cotización tiene una fecha de vencimiento en {{$exp_date}}.
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td align="center" style="font-size:0px;padding:0 25px;padding-top:40px;word-break:break-word;">

                              <div style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;;font-size:14px;line-height:1;text-align:center;color:#585858;">
                                Best,
                                <br> The kicktask Team
                                <p></p>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>

</html>