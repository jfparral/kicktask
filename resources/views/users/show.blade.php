@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Detalles del Usuario'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('users.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Usuarios</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('users.show',['$user'])}}" class="m-nav__link">
        <span class="m-nav__link-text">{{$user->name}}</span>
    </a>
</li>
@slot('options')
<a href="{{route('users.index')}}" class="btn btn-success">Regresar</a>
@endslot @endbreadcrumbs
<!-- END: Subheader -->

<!--Section-->
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div class="m-portlet__body mb-0">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Nombre:</h3>
                                        <span class="m--font-boldest">{{$user->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Cédula:</h3>
                                        <span class="m--font-boldest">{{$user->legal_id}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Teléfono:</h3>
                                        <span class="m--font-boldest">{{$user->phone}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Correo:</h3>
                                        <span class="m--font-boldest">{{$user->email}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Dirección:</h3>
                                        <span class="m--font-boldest">{{$user->address}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection