@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Editar Usuario'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("clients.index") }}" class="m-nav__link">
        <span class="m-nav__link-text">Usuarios</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("users.edit", $user) }}" class="m-nav__link">
        <span class="m-nav__link-text">Editar</span>
    </a>
</li>
@slot('options')
@endslot 
@endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-portlet col mb-0">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text"> Ingrese los detalles del usuario</h3>
                            </div>
                        </div>
                    </div>
                    <form class="m-form m-form--fit m-form--label-align-right m-form--state col-md-6 pl-0" method="post" action="{{route('users.update', $user)}}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                            <label for="name"> Nombre:</label>
                            <input  id="name" class="form-control m-input m-input--square" name="name" value="{{ $user->name }}"  placeholder="Ingrese Nombre">
                            @if($errors->has('name'))
                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                            <label for="email"> Correo:</label>
                            <input type="email" id="email" class="form-control m-input m-input--square" name="email" aria-describedby="emailHelp" value="{{ $user->email }}" placeholder="Ingrese correo">
                            @if($errors->has('email'))
                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('password')) has-danger @endif">
                            <label for="password"> Contraseña:</label>
                            <input type="password" id="password" class="form-control m-input m-input--square" name="password" aria-describedby="password" placeholder="Ingrese contraseña">
                            @if($errors->has('password'))
                                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('phone')) has-danger @endif">
                            <label for="phone"> Teléfono:</label>
                            <input type="text" id="phone" value="{{ $user->phone }}" class="form-control m-input m-input--square" name="phone" placeholder="Ingrese teléfono">
                            @if($errors->has('phone'))
                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('legal_id')) has-danger @endif">
                            <label for="legal_id"> Cédula/R.U.C:</label>
                            <input type="text" value="{{ $user->legal_id }}" class="form-control m-input m-input--square" name="legal_id" placeholder="Ingrese cédula o R.U.C">
                            @if($errors->has('legal_id'))
                                <div class="form-control-feedback">{{ $errors->first('legal_id') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('address')) has-danger @endif">
                            <label for="address"> Dirección:</label>
                            <input type="text" value="{{ $user->address }}" class="form-control m-input m-input--square" name="address" placeholder="Ingrese dirección">
                            @if($errors->has('address'))
                                <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('role_id')) has-danger @endif">
                            <label for="role_id"> Rol:</label>
                            <select name="role_id" id="role_id" class="selectpicker bootstrap-select form-control m-bootstrap-select">
                                <option value="">Seleccionar rol</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" @if($user->role->id === $role->id) selected @endif>{{ $role->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('role_id'))
                                <div class="form-control-feedback">{{ $errors->first('role_id') }}</div>
                            @endif
                        </div>

                        <div class="form-group m-form__group @if($errors->has('clients')) has-danger @endif">
                            <label for="clients"> Clientes:</label>
                            <select class="form-control m-select2" id="clients" name="clients[]" multiple="multiple">
                                <option></option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}" @if($user->hasClient($client->id)) selected @endif>{{ $client->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('clients'))
                                <div class="form-control-feedback">{{ $errors->first('clients') }}</div>
                            @endif
                        </div>
                        <div class="m-form__actions">
                            <a href="{{ route("users.index")}}" class="btn btn-metal">Cancelar</a>
                            <button type="submit"  class="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('#clients').select2({
            placeholder: "Seleccionar clientes",
        });
    </script>
@endpush