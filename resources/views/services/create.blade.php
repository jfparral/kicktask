@extends('layouts.layout') 

@section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Agregar Servicio'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('clients.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">{{$client->name}}</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('services.create',[$client])}}" class="m-nav__link">
        <span class="m-nav__link-text">Agregar Servicio</span>
    </a>
</li>
@slot('options')
@endslot 
@endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                    <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Detalles del Servicio	
                    </h3>
                </div>
            </div>
        </div>
        <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="post" action="{{route('services.store',[$client])}}">
        @csrf
            <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                <label for="resource_id">Seleccione Recurso:</label>
                <select class="form-control m-input m-input--square col-lg-5" name="resource_id" id="resource_id">
                    <option value="">Recursos</option>
                    @foreach($resources as $resource)
                        <option value="{{$resource->id}}">{{$loop->iteration}}.-  {{$resource->name}}</option>
                    @endforeach
                </select>
                @if($errors->has('resource_id'))
                    <div class="form-control-feedback">{{ $errors->first('resource_id') }}</div>
                @endif
            </div>
            <div class="row"> 
                <div class="form-group m-form__group @if($errors->has('name')) has-danger @elseif($errors->has('name')) has-danger @endif">
                    <label for="name">Nombre:</label>
                    <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" name="name" placeholder="Ingrese nombre">
                    @if($errors->has('name'))
                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group m-form__group @if($errors->has('hours')) has-danger @elseif($errors->has('hours')) has-danger @endif">
                    <label for="hours">Horas:</label>
                    <input id="hours" type="text" class="form-control form-control-danger m-input m-input--square" name="hours" placeholder="Ingrese horas">
                    @if($errors->has('hours'))
                        <div class="form-control-feedback">{{ $errors->first('hours') }}</div>
                    @endif
                </div>
                <div class="col-lg-12 form-group m-form__group pb-0 @if($errors->has('description')) has-danger @endif">
                    <label for="description">Detalle:</label>
                    <textarea name="description" id="description" class="form-control form-control-danger m-input m-input--square" cols="80" rows="5" placeholder="Ingrese descripción"></textarea>
                    @if($errors->has('description'))
                        <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                    @endif
                </div>
                <div class="col-lg-12 m-form__actions m--align-right pb-0">
                    <a href="#" class="btn btn-success">Agregar Detalle</a>
                </div>
            </div>
            <div class="m-form__actions">
                <a href="{{ route("clients.show",[$client])}}" class="btn btn-metal">Cancelar</a>
                <button type="submit"  class="btn btn-success">Guardar</button>
            </div>
        </form>
    </div>
</div>

@endsection