@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Agregar Cliente'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("clients.index") }}" class="m-nav__link">
        <span class="m-nav__link-text">Clientes</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("clients.create") }}" class="m-nav__link">
        <span class="m-nav__link-text">Crear</span>
    </a>
</li>
@slot('options')
@endslot 
@endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-portlet col mb-0">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text"> Ingrese los detalles del cliente</h3>
                            </div>
                        </div>
                    </div>
                    <form class="m-form m-form--fit m-form--label-align-right m-form--state col-md-6 pl-0" method="post" action="{{route('clients.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                            <label for="name"> Nombre:</label>
                            <input  id="name" type="text" class="form-control m-input m-input--square" name="name"  placeholder="Ingrese Nombre">
                            @if($errors->has('name'))
                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('comercial_name')) has-danger @endif">
                            <label for="comercial_name"> Nombre Comercial:</label>
                            <input  id="comercial_name" type="text" class="form-control m-input m-input--square" name="comercial_name"  placeholder="Ingrese Nombre Comercial">
                            @if($errors->has('comercial_name'))
                                <div class="form-control-feedback">{{ $errors->first('comercial_name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                            <label for="email"> Correo:</label>
                            <input type="email" id="email" class="form-control m-input m-input--square" name="email" aria-describedby="emailHelp" placeholder="Ingrese correo">
                            @if($errors->has('email'))
                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('address')) has-danger @endif">
                            <label for="address"> Dirección:</label>
                            <input  type="text" id="address" class="form-control m-input m-input--square" name="address" placeholder="Ingrese dirección">
                            @if($errors->has('address'))
                                <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('phone')) has-danger @endif">
                            <label for="phone"> Teléfono:</label>
                            <input  class="form-control m-input m-input--square" name="phone" placeholder="Ingrese teléfono">
                            @if($errors->has('phone'))
                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('legal_id')) has-danger @endif">
                            <label for="legal_id"> RUC:</label>
                            <input  id="legal_id"  type="text" class="form-control m-input m-input--square" name="legal_id" placeholder="Ingrese RUC">
                            @if($errors->has('legal_id'))
                                <div class="form-control-feedback">{{ $errors->first('legal_id') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group">
                            <div class="custom-file">
                                <input type="file"  name="logo" class="custom-file-input" id="customFile" aria-describedby="fileHelp">
                                <label class="custom-file-label" for="customFile">Subir logo</label>
                            </div>
                        </div>
                        <div class="m-form__actions">
                            <a href="{{ route("clients.index")}}" class="btn btn-metal">Cancelar</a>
                            <button type="submit"  class="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection