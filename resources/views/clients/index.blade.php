@extends('layouts.layout') 

@section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Clientes'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('clients.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Clientes</span>
    </a>
</li>
@slot('options')
<a href="{{ route('clients.create') }}" class="btn btn-primary">Agregar cliente</a>
@endslot @endbreadcrumbs
<!-- END: Subheader -->

<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section_content">
                        <table class="table table-bordered m-table m-table--head-bg-brand mb-0">
                            <thead>
                                <tr>
                                    <th class="m--img-centered">#</th>
                                    <th class="m--img-centered">Logo</th>
                                    <th class="m--img-centered">Nombre</th>
                                    <th class="m--img-centered">Teléfono</th>
                                    <th class="m--img-centered">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($clients as $client)
                                <tr>
                                    <td class="m--img-centered" >{{$loop->iteration}}</td>
                                    <td class="m--img-centered"><img src="{{$client->logo}}" class="m--img-centered" alt="" height="35"></td>
                                    <td class="m--img-centered">{{$client->name}}</td>
                                    <td class="m--img-centered">{{$client->phone}}</td>
                                    <td class="m--img-centered">
                                        <a href="{{ route('clients.show', [$client])}}" class="btn btn-primary btn-sm">
                                            <i class="m-menu__link-icon flaticon-visible"></i>
                                            Ver
                                        </a>
                                        <a href="{{ route('clients.edit', [$client])}}" class="btn btn-success btn-sm">
                                            <i class="m-menu__link-icon flaticon-edit"></i>
                                            Editar
                                        </a>

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$client->id}}">
                                            <i class="m-menu__link-icon flaticon-circle"></i>
                                            Eliminar
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminarModal{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Cliente {{ $client->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Está seguro que desea eliminar este cliente?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        <form action="{{ route('clients.destroy', [$client])}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-primary" type="submit">
                                                                Eliminar
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection