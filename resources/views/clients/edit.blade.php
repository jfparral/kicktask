@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Editar Cliente'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("clients.index") }}" class="m-nav__link">
        <span class="m-nav__link-text">Clientes</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{ route("clients.edit",[$client]) }}" class="m-nav__link">
        <span class="m-nav__link-text">Editar</span>
    </a>
</li>
@slot('options')
<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
    aria-expanded="true">
    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
        <i class="la la-plus m--hide"></i>
        <i class="la la-ellipsis-h"></i>
    </a>
    <div class="m-dropdown__wrapper">
        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
        <div class="m-dropdown__inner">
            <div class="m-dropdown__body">
                <div class="m-dropdown__content">
                    <ul class="m-nav">
                        <li class="m-nav__section m-nav__section--first m--hide">
                            <span class="m-nav__section-text">Quick Actions</span>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-share"></i>
                                <span class="m-nav__link-text">Activity</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                <span class="m-nav__link-text">Messages</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-info"></i>
                                <span class="m-nav__link-text">FAQ</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                <span class="m-nav__link-text">Support</span>
                            </a>
                        </li>
                        <li class="m-nav__separator m-nav__separator--fit">
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endslot @endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text"> Ingrese los detalles del cliente</h3>
                            </div>
                        </div>
                    </div>
                    <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="POST" action="{{ route('clients.update', [$client]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group m--margin-top-10">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                        "Ingrese la información del nuevo cliente que se desea guardar, tenga en cuenta que el campo de RUC/Cédula solo acepta 13
                                        dígitos."
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                            <label for="name"> Nombre:</label>
                            <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" value="{{$client->name}}" name="name"  placeholder="Ingrese Nombre">
                            @if($errors->has('name'))
                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('comercial_name')) has-danger @endif">
                            <label for="comercial_name"> Nombre Comercial:</label>
                            <input  id="comercial_name" type="text" class="form-control m-input m-input--square" value="{{$client->comercial_name}}" name="comercial_name"  placeholder="Ingrese Nombre Comercial">
                            @if($errors->has('comercial_name'))
                                <div class="form-control-feedback">{{ $errors->first('comercial_name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('email')) has-danger @endif">
                            <label for="email"> Correo:</label>
                            <input type="email" class="form-control m-input m-input--square" value="{{$client->email}}" name="email" aria-describedby="emailHelp" placeholder="Ingrese correo">
                            @if($errors->has('email'))
                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('address')) has-danger @endif">
                            <label for="address"> Dirección:</label>
                            <input  class="form-control m-input m-input--square" name="address" type="text" value="{{$client->address}}" placeholder="Ingrese dirección">
                            @if($errors->has('address'))
                                <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('phone')) has-danger @endif">
                            <label for="phone"> Teléfono:</label>
                            <input  type="text" class="form-control m-input m-input--square" name="phone" value="{{$client->phone}}" placeholder="Ingrese teléfono">
                            @if($errors->has('phone'))
                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group @if($errors->has('legal_id')) has-danger @endif">
                            <label for="ruc"> RUC:</label>
                            <input  type="text" class="form-control m-input m-input--square" name="legal_id" value="{{$client->legal_id}}" placeholder="Ingrese RUC">
                            @if($errors->has('legal_id'))
                                <div class="form-control-feedback">{{ $errors->first('legal_id') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group">
                            <div class="custom-file">
                                <label for="file"> Subir Logo:</label>
                                <input type="file"  name="logo" class="custom-file-input" id="customFile" aria-describedby="fileHelp">
                                <label class="custom-file-label" for="customFile">Subir logo</label>
                            </div>
                        </div>
                        <div class="m-form__actions">
                            <a href="{{ route("clients.index")}}" class="btn btn-metal">Cancelar</a>
                            <button type="submit"  class="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection