@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Detalles del Cliente'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('clients.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Clientes</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('clients.show',[$client])}}" class="m-nav__link">
        <span class="m-nav__link-text">{{$client->name}}</span>
    </a>
</li>
@slot('options')
<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
    aria-expanded="true">
    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
        <i class="la la-plus m--hide"></i>
        <i class="la la-ellipsis-h"></i>
    </a>
    <div class="m-dropdown__wrapper">
        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
        <div class="m-dropdown__inner">
            <div class="m-dropdown__body">
                <div class="m-dropdown__content">
                    <ul class="m-nav">
                        <li class="m-nav__section m-nav__section--first m--hide">
                            <span class="m-nav__section-text">Quick Actions</span>
                        </li>
                        <li class="m-nav__item">
                            <a href="{{route('clients.create')}}" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-share"></i>
                                <span class="m-nav__link-text">Agregar Cliente</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                <span class="m-nav__link-text">Messages</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-info"></i>
                                <span class="m-nav__link-text">FAQ</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                <span class="m-nav__link-text">Support</span>
                            </a>
                        </li>
                        <li class="m-nav__separator m-nav__separator--fit">
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endslot @endbreadcrumbs
<!-- END: Subheader -->

<!--Section-->
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div class="m-portlet__head mb-0">
                <div class="m-content pl-0 pr-0">
                    <div class="row">
                        <div class="m--align-left">
                            <img src="{{$client->logo}}" class="-m--img-rounded m--img-centered" alt="" height="50">
                        </div>
                        <div class="col-lg-6 d-flex align-items-center">
                            <h4 class="mb-0">{{$client->name}}</h4>
                        </div>
                        <div class="col-lg m--align-right">
                            <a href="{{route('clients.index')}}" class="btn btn-success">Regresar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body mb-0">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Nombre Comercial:</h3>
                                        <span class="m--font-boldest">{{$client->comercial_name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">RUC:</h3>
                                        <span class="m--font-boldest">{{$client->legal_id}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Teléfono:</h3>
                                        <span class="m--font-boldest">{{$client->phone}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Correo:</h3>
                                        <span class="m--font-boldest">{{$client->email}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Dirección:</h3>
                                        <span class="m--font-boldest">{{$client->address}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mportlet-head">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3>Servicios del Cliente</h3>
                        </div>
                        <div class="col-lg-6 m--align-right">
                            <a href="{{ route('services.create',['id' => $client->id])}}" class="btn btn-primary">Agregar Servicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-section">
                <div class="m-section__content">
                    <table class="table table-bordered m-table m-table--head-bg-brand">
                        <thead>
                            <tr>
                                <th class="m--img-centered">#</th>
                                <th class="m--img-centered">Área</th>
                                <th class="m--img-centered">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td class="m--img-centered"> {{$loop->iteration}} </td>
                                <td class="m--img-centered">{{$service->resource->name}}</td>
                                <td class="m--img-centered">
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showmodalint{{$service->id}}">
                                        <i class="m-menu__link-icon flaticon-visible"></i>
                                    </button>

                                    <!-- Button trigger edit modal -->
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editmodalint{{$service->id}}">
                                        <i class="m-menu__link-icon flaticon-edit"></i>
                                    </button>

                                    <!-- Button trigger modal destroy -->
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#destroymodalint{{$service->id}}">
                                        <i class="la la-trash-o"></i>
                                    </button>

                                    <!-- Modal Show-->

                                    <div class="modal fade show" role="document" id="showmodalint{{$service->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-lg m--align-left">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Detalles de {{$service->resource->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    @foreach($service->servicesdetails as $service_detail)
                                                    <div class="m-stack m-stack--hor m-stack--general m-stack--demo">
                                                        <form action="{{route('services_detail.destroy',[$service_detail])}}" method="post">
                                                            @csrf @method('delete')
                                                            <div class="m-stack__item">
                                                                <div class="d-flex align-items-center">
                                                                    <div class="mr-auto" style="align-content: center; padding-top:20px padding-bottom:20px">
                                                                        <div class="m-section__heading" style="font-weight: 800;">
                                                                            Detalle de {{$service_detail->name}}
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <button type="submit" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                                                            <i class="la la-trash-o"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="m-stack__demo-item" style="text-align: justify">
                                                                <span class="m--font-boldest">Descripción:</span>
                                                                {{$service_detail->description}}
                                                            </div>
                                                            <div class="m-stack__demo-item">
                                                                <span class="m--font-boldest">Horas:</span>
                                                                <span>{{$service_detail->hours}}</span>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div style="margin-top:30px;margin-bottom:10px;"></div>
                                                    @endforeach
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal Edit-->

                                    <div class="modal fade show" role="document" id="editmodalint{{$service->id}}"
                                        tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg m--align-left">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Subservicios de {{$service->resource->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="d-flex align-items-center">
                                                        <div class="mr-auto"></div>
                                                        <div>
                                                            <button class="btn btn-outline-accent m-btn m-btn--custom m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air btn-sm">
                                                                <span>
                                                                    <i class="fa fa-plus"></i>
                                                                    <span>Agregar Subservicio</span>
                                                                </span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    @foreach($service->servicesdetails as $service_detail)
                                                        <form class="m-form" action="{{ route('services_detail.update',[$service_detail])}}" method="post" role="form">
                                                            @csrf @method('patch')
                                                            <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                <label for="name"> Nombre:</label>
                                                                <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" value="{{$service_detail->name}}"
                                                                    name="name" placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                <label class="col-sm-2 control-label" for="description">Descripción:</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="description" class="form-control" id="description" value="" placeholder="Ingrese descripción" name="description"
                                                                        cols="30" rows="5">{{$service_detail->description}}</textarea>
                                                                    @if($errors->has('description'))
                                                                    <div class="form-control-feedback">{{ $errors->first('description') }}
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group m-form__group @if($errors->has('hours')) has-danger @endif">
                                                                <label for="hours"> Horas de Trabajo:</label>
                                                                <input id="hour_update" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$service_detail->hours}}"
                                                                    name="hours" placeholder="Ingrese hora"> @if($errors->has('hours'))
                                                                <div class="form-control-feedback">{{ $errors->first('hours') }}</div>
                                                                @endif
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="m-form__actions">
                                                                    <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-success">Guardar</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal Destroy-->

                                    <div class="modal fade" id="destroymodalint{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog m--align-left" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Servicio de {{ $service->resource->name }} de {{ $service->client->name
                                                        }}
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    ¿Está seguro que desea eliminar este servicio incluyendo todos sus subservicios?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                    <form action="{{ route('services.destroy', [$service])}}" method="post">
                                                        @csrf @method('delete')
                                                        <button class="btn btn-danger" type="submit">Eliminar Todo</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection