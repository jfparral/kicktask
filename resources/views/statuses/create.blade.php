@extends('layouts.layout')

@section('content')
    <!-- BEGIN: Subheader -->
    @breadcrumbs(['title' => 'Agregar Estado'])
        <li class="m-nav__item m-nav__item--home">
            <a href="/" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i>
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{{ route("statuses.index") }}" class="m-nav__link">
                <span class="m-nav__link-text">Estados</span>
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{{ route("statuses.create") }}" class="m-nav__link">
                <span class="m-nav__link-text">Crear</span>
            </a>
        </li>
        @slot('options')
        @endslot 
    @endbreadcrumbs
    <!-- END: Subheader -->
    <div class="m-content">
        <!--Section-->
        <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="m-portlet col mb-0">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text"> Ingrese los detalles del estado</h3>
                                </div>
                            </div>
                        </div>
                        <form class="m-form m-form--fit m-form--label-align-right m-form--state col-md-6 pl-0" method="post" action="{{ route('statuses.store') }}">
                            @csrf
                            <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                <label for="name"> Nombre:</label>
                                <input  id="name" class="form-control m-input m-input--square" name="name"  placeholder="Ingrese Nombre">
                                @if($errors->has('name'))
                                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                            <div class="m-form__actions">
                                <a href="{{ route("statuses.index")}}" class="btn btn-metal">Cancelar</a>
                                <button type="submit"  class="btn btn-success">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--  @push('scripts')
    <script>
        $('#statuses').select2({
            placeholder: "Seleccionar clientes",
        });
    </script>
@endpush  --}}