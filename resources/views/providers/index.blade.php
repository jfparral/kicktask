@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Tareas de Proveedor'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('providers.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas de Proveedor</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<!-- END: Subheader -->

<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section_content">
                        <table class="table table-bordered m-table m-table--head-bg-brand mb-0">
                            <thead>
                                <tr>
                                    <th class="m--img-centered">#</th>
                                    <th class="m--img-centered">Proveedor</th>
                                    <th class="m--img-centered">Estado</th>
                                    <th class="m--img-centered">Fecha de Entrega</th>
                                    <th class="m--img-centered">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($providers as $provider)
                                <tr>
                                    <td class="m--img-centered">{{$loop->iteration}}</td>
                                    <td class="m--img-centered">{{$provider->name_provider}}</td>
                                    <td class="m--img-centered">{{$provider->status->name}}</td>
                                    <td class="m--img-centered">{{$provider->due_date}}</td>
                                    <td class="m--img-centered">
                                        <a href="{{ route('providers.show', [$provider])}}" class="btn btn-primary btn-sm">
                                            <i class="m-menu__link-icon flaticon-visible"></i>
                                        </a>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editModal{{$provider->id}}">
                                            <i class="m-menu__link-icon flaticon-edit"></i>
                                        </button>

                                        <!-- Modal Edit-->
                                        <div class="modal fade" id="editModal{{$provider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="POST" action="{{ route('providers.update', [$provider]) }}">
                                                    @csrf @method('PATCH')
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Editar tarea de {{ $provider->name_provider }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group m-form__group @if($errors->has('due_date')) has-danger @endif" style="text-align:left">
                                                                <label for="date"> Fecha de Entrega:</label>
                                                                <input name="due_date" class="form-control date col-lg-5"  style="text-align:left" type="date" value="{{$provider->due_date}}" placeholder="Ingrese fecha"
                                                                    readonly id="date"> @if($errors->has('due_date'))
                                                                <div class="form-control-feedback">{{ $errors->first('due_date') }}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                            <button  type="submit" class="btn btn-primary">Actualizar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$provider->id}}">
                                            <i class="m-menu__link-icon flaticon-circle"></i>
                                        </button>

                                        <!-- Modal Destroy-->
                                        <div class="modal fade" id="eliminarModal{{$provider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar providere {{ $provider->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Está seguro que desea eliminar esta tarea del proveedor {{$provider->contact->name}}?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        <form action="{{ route('providers.destroy', [$provider])}}" method="POST">
                                                            @csrf @method('delete')
                                                            <button class="btn btn-danger">
                                                                Eliminar
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('.date').datepicker({
        orientation: "bottom left",
        autoclose: true,
        format: "yyyy-mm-dd",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
</script>
@endpush