@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Detalles de Tarea'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('providers.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Tareas de Proveedores</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('providers.show',[$provider])}}" class="m-nav__link">
        <span class="m-nav__link-text">{{$provider->name_provider}}</span>
    </a>
</li>
@slot('options')
<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
    aria-expanded="true">
    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
        <i class="la la-plus m--hide"></i>
        <i class="la la-ellipsis-h"></i>
    </a>
</div>
@endslot @endbreadcrumbs
<!-- END: Subheader -->

<!--Section-->
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div class="m-portlet__head mb-0">
                <div class="m-content pl-0 pr-0">
                    <div class="row">
                        <div class="col-lg-6 d-flex align-items-center">
                            <h4 class="mb-0">{{$provider->name_provider}}</h4>
                        </div>
                        <div class="col-lg m--align-right">
                            <a href="{{route('providers.index')}}" class="btn btn-success">Regresar</a>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__body mb-0">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Correo:</h3>
                                        <span class="m--font-boldest">{{$provider->contact->email}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Teléfono:</h3>
                                        <span class="m--font-boldest">{{$provider->contact->phone}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Descripción de la Tarea:</h3>
                                        <span class="m--font-boldest">{{$provider->description}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Estado:</h3>
                                        <span class="m--font-boldest">{{$provider->status->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Fecha de Entrega:</h3>
                                        <span class="m--font-boldest">{{$provider->due_date}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection