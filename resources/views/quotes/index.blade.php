@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Cotizaciones'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('quotes.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Cotizaciones</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<div class="m-content">
    <!--Widgets-->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Total Cotizaciones
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Todas
                            </span>
                            <span class="m-widget24__stats m--font-brand">
                                15
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                Carga
                            </span>
                            <span class="m-widget24__number">
                                78%
                            </span>
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Feedbacks-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Total abiertas
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Cotizaciones abiertas
                            </span>
                            <span class="m-widget24__stats m--font-info">
                                3
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                Carga
                            </span>
                            <span class="m-widget24__number">
                                84%
                            </span>
                        </div>
                    </div>
                    <!--end::New Feedbacks-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Total a Cotizar
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Cotizar
                            </span>
                            <span class="m-widget24__stats m--font-danger">
                                5
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                Carga
                            </span>
                            <span class="m-widget24__number">
                                69%
                            </span>
                        </div>
                    </div>
                    <!--end::New Orders-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Users-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                Cotizaciones Aprobadas
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
                                Aprobadas
                            </span>
                            <span class="m-widget24__stats m--font-success">
                                7
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="m-widget24__change">
                                Carga
                            </span>
                            <span class="m-widget24__number">
                                90%
                            </span>
                        </div>
                    </div>
                    <!--end::New Users-->
                </div>
            </div>
        </div>
    </div>
    <!--Filters-->

    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section__content">
                        <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="GET" action="{{ route('quotes.index') }}">
                            <div class="row d-flex p-4">
                                <div class="col-lg-3">
                                    <label for="client_id">Cliente:</label>
                                    <select class="form-control selectpicker" name="client_id" id="client_id">
                                        <option value=""> Seleccionar cliente</option>
                                        @foreach($clients as $client)
                                        <option value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label for="status_id">Estado:</label>
                                    <select class="form-control selectpicker" name="status_id" id="status_id">
                                        <option value=""> Seleccionar estado</option>
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2 align-items-center">
                                    <label for="from">Desde:</label>
                                    <input type="text" class="form-control date" name="from" id="from" readonly placeholder="Fecha desde" />
                                </div>
                                <div class="col-lg-2 align-items-center">
                                    <label for="to">Hasta:</label>
                                    <input type="text" class="form-control date" name="to" id="to" readonly placeholder="Fecha hasta" />
                                </div>
                                <div class="col-lg-2 d-flex align-items-end">
                                    <button class="btn btn-success">Filtrar</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-bordered m-table m-table--head-bg-brand">
                            <thead>
                                <tr>
                                    <th class="m--img-centered">#</th>
                                    <th class="m--img-centered">Cliente</th>
                                    <th class="m--img-centered">Estado</th>
                                    <th class="m--img-centered">Fecha de Expiración</th>
                                    <th class="m--img-centered">Subtotal</th>
                                    <th class="m--img-centered">IVA</th>
                                    <th class="m--img-centered">Total</th>
                                    <th class="m--img-centered">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($quotes as $quote)
                                <tr>
                                    <td class="m--align-center">{{$loop->iteration}}</td>
                                    <td class="m--align-center">{{$quote->client_name}}</td>
                                    <td class="m--align-center"><span class="m-badge m-badge--accent m-badge--wide">{{$quote->status->name}}</span></td>
                                    <td class="m--align-center">{{$quote->exp_date}}</td>
                                    <td class="m--align-center">{{$quote->subtotal}}</td>
                                    <td class="m--align-center">{{$quote->tax_total}}</td>
                                    <td class="m--align-center">{{$quote->total}}</td>
                                    <td class="m--align-center">
                                        <a href="{{ route('quotes.show', [$quote])}}" class="btn btn-success btn-sm">
                                            <i class="m-menu__link-icon flaticon-visible"></i>
                                            Ver
                                        </a>
                                        <a href="{{ route('quotes.edit', [$quote])}}" class="btn btn-info btn-sm">
                                            <i class="m-menu__link-icon flaticon-edit"></i>
                                            Editar
                                        </a>
                                        @if($quote->status->name!='Enviada' && $quote->status->name!='Borrador')
                                            <a href="{{route('email.send',[$quote])}}" class="btn btn-warning btn-sm">
                                                <i class="m-menu__link-icon flaticon-paper-plane"></i>
                                                Enviar Cotización
                                            </a>
                                        @endif

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$quote->id}}">
                                            <i class="m-menu__link-icon flaticon-circle"></i> Eliminar</button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminarModal{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Cotización {{ $quote->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Está seguro que desea eliminar esta cotización?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        <form action="{{ route('quotes.destroy', [$quote])}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-primary" type="submit">
                                                                Eliminar
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.date').datepicker({
            orientation: "bottom left",
            autoclose: true,
            format: "yyyy-mm-dd",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#client_id').select2({
            placeholder: "Seleccionar cliente"
        });
        $('#status_id').select2({
            placeholder: "Seleccionar estado"
        });
    </script>
@endpush