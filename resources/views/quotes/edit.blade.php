@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Crear Cotización'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('quotes.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Cotizaciones</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('quotes.edit',[$quote])}}" class="m-nav__link">
        <span class="m-nav__link-text">Editar</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<div class="m-content">
    <div class="m-porlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Datos Generales de la Cotización</h3>
                    </div>
                </div>
            </div>
            <div class="m-porlet__body">
                <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="post" action="{{route('quotes.update',[$quote])}}"
                    encresource="multipart/form-data">
                    @csrf @method('patch')
                    <!-- General Data: Quote -->
                    <div class="form-group m-form__group">
                        <div class="row">
                            <div class="m--align-left">
                                <img src="{{$quote->client->logo}}" class="-m--img-rounded m--img-centered" alt="" height="50">
                            </div>
                            <div class="col-lg-6 d-flex align-items-center">
                                <h3 class="mb-0">{{$quote->client->name}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="contact">Contacto</label>
                        <label for="contact_name">{{$quote->contact->name}}</label>
                    </div>
                    <div class="form-group m-form__group @if($errors->has('terms')) has-danger @endif">
                        <label for="terms">Términos Legales:</label>
                        <textarea class="form-control m-input summernote col-lg-8" name="terms" id="terms" cols="80" rows="5" placeholder="Términos y Condiciones para el Cliente.">{{$quote->terms}}</textarea>
                        @if($errors->has('terms'))
                        <div class="form-control-feedback">{{ $errors->first('terms') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('status_id')) has-danger @endif">
                        <label for="status_id">Estado:</label>
                        <select class="form-control selectpicker col-sm-5" name="status_id" id="status_id">
                            <option value="">Seleccionar estado</option>
                            @foreach($statuses as $status)
                            <option value="{{$status->id}}" @if($status->id === $quote->status_id)selected @endif>{{$status->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('status_id'))
                        <div class="form-control-feedback">{{ $errors->first('status_id') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('exp_date')) has-danger @endif">
                        <label for="exp_date">Fecha de Expiración</label>
                        <input name="exp_date" class="form-control date col-lg-3" readonly type="date" value="{{$quote->exp_date}}" id="exp_date"> @if($errors->has('exp_date'))
                        <div class="form-control-feedback">{{ $errors->first('exp_date') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group @if($errors->has('payment')) has-danger @endif">
                        <label for="payment">Forma de Pago</label>
                        <textarea class="form-control m-input col-lg-8" name="payment" id="payment" cols="80" rows="5" placeholder="Forma de Pago.">{{$quote->payment}}</textarea>
                        @if($errors->has('payment'))
                        <div class="form-control-feedback">{{ $errors->first('payment') }}</div>
                        @endif
                    </div>
                    <div class=" form-group m-form__actions">
                        <div class="row">
                            <div class="col-lg-2">
                                <a class="btn btn-metal" href="{{route('quotes.index')}}">Cancelar</a>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-success" type="submit">Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="m-porlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Detalles de la Cotización</h3>
                    </div>
                </div>
            </div>
            <div class="m-porlet__body">
                <!-- Tabs Noucht-->
                <ul class="nav nav-pills nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rec_intern">Recursos Internos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rec_extern">Recursos Externos</a>
                    </li>
                </ul>
                <!-- Tabs Body-->
                <div class="tab-content">
                    <div class="tab-pane" id="rec_intern" role="tabpanel">
                        <div class="m-porlet">
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="m-section col mb-0">
                                        <div class="m-section_content">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title d-flex align-items-center">
                                                        <div class="row col">
                                                            <div class="col-lg-6 d-flex align-items-center">
                                                                <h3 class="m-portlet__head-text"> Ingrese los detalles de la cotización</h3>
                                                            </div>
                                                            <!-- Button trigger create modal intern -->
                                                            <div class="col-lg-6 m--align-right">
                                                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#createModal{{$quote->id}}">
                                                                    <i class="m-menu__link-icon flaticon-add"></i>
                                                                    Agregar Detalle Interno
                                                                </button>
                                                                <!-- Modal Create Intern-->
                                                                <div class="modal fade show" role="document" id="createModal{{$quote->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                    aria-hidden="true">
                                                                    <div class="modal-dialog modal-lg m--align-left">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">Detalle Interno</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">×</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form class="m-form" action="{{route('quotes_details.store',[$quote])}}" custom="custom" method="post" role="form">
                                                                                    @csrf
                                                                                    <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                                        <label for="name"> Nombre:</label>
                                                                                        <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" value="" name="name" placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                                        <label for="description">Descripción:</label>
                                                                                        <div class="col-sm-10">
                                                                                            <textarea type="description" class="form-control m-input" id="description" value="" placeholder="Ingrese descripción" name="description"
                                                                                                cols="30" rows="5"></textarea>
                                                                                            @if($errors->has('description'))
                                                                                            <div class="form-control-feedback">{{ $errors->first('description')}}</div>
                                                                                            @endif
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('type_id')) has-danger @endif">
                                                                                    <label for="type_id">Tipo:</label>
                                                                                    <select class="form-control selectpicker col-sm-5" name="type_id" id="type_id">
                                                                                        <option value="">Seleccionar proveedor</option>
                                                                                        @foreach($types as $type)
                                                                                        <option value="">{{$type->name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @if($errors->has('type_id'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('type_id') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                    <input type="text" hidden name='type' value='intern'>
                                                                                    <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                                                                                        <label for="resource_id">Recurso:</label>
                                                                                        <select class="form-control selectpicker col-sm-5" name="resource_id" id="resource_id">
                                                                                            <option value="">Seleccionar recurso</option>
                                                                                            @foreach($resources as $resource)
                                                                                            <option value="{{$resource->id}}">{{$resource->name}}
                                                                                            </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                        @if($errors->has('resource_id'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('resource_id')
                                                                                            }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('hour')) has-danger @endif">
                                                                                        <label for="hour"> Horas de Trabajo:</label>
                                                                                        <input id="hour_work" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="" name="hours"
                                                                                            placeholder="Ingrese hora"> @if($errors->has('hour'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('hour') }}</div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group">
                                                                                        <label for="cost"> Costo:</label>
                                                                                        <span id='cost' name="cost" class="m-badge m-badge--warning m-badge--wide"></span>
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('gain_percent')) has-danger @endif">
                                                                                        <label for="gain_percent"> Porcentaje de Margen:</label>
                                                                                        <input id="gain_percent" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="" name="gain_percent"
                                                                                            placeholder="Ingrese porcentaje"> @if($errors->has('gain_percent'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('gain_percent')
                                                                                            }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group">
                                                                                        <label for="gain"> Margen:</label>
                                                                                        <span id='gain' name="gain" class="m-badge m-badge--success m-badge--wide"></span>
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('total')) has-danger @endif">
                                                                                        <label for="total"> Total:</label>
                                                                                        <span id="total_label" name="total" class="m-badge m-badge--danger m-badge--wide"></span>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <div class="m-form__actions">
                                                                                            <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                                            <button type="submit" class="btn btn-success">Guardar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered m-table m-table--head-bg-brand">
                                                <thead>
                                                    <tr>
                                                        <th class="m--img-centered">#</th>
                                                        <th class="m--img-centered">Nombre</th>
                                                        <th class="m--img-centered">Recurso</th>
                                                        <th class="m--img-centered">Costo de Venta</th>
                                                        <th class="m--img-centered">% Margen</th>
                                                        <th class="m--img-centered">Margen</th>
                                                        <th class="m--img-centered">Precio de Venta</th>
                                                        <th class="m--img-centered">Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($quotes_details_intern as $quote_detail)
                                                    <tr>
                                                        <td class="m--align-center">{{$loop->iteration}}</td>
                                                        <td class="m--align-center">{{$quote_detail->name}}</td>
                                                        <td class="m--align-center">{{$quote_detail->resource->name}}</td>
                                                        <td class="m--align-center">$ {{$quote_detail->cost}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain_percent}} %</td>
                                                        <td class="m--align-center">$ {{$quote_detail->gain}}</td>
                                                        <td class="m--align-center">$ {{$quote_detail->total}}</td>
                                                        <td class="m--align-center">
                                                            <!-- Button trigger show modal -->
                                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showmodalint{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-visible"></i>
                                                                Ver
                                                            </button>

                                                            <!-- Button trigger edit modal -->
                                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editmodalint{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-edit"></i>
                                                                Editar
                                                            </button>

                                                            <!-- Button trigger modal destroy -->
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#destroymodalint{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-circle"></i>
                                                                Eliminar
                                                            </button>

                                                            <!-- Modal Show-->
                                                            <div class="modal fade show" role="document" id="showmodalint{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->name}}"</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Descripción:
                                                                                            <span class="m-badge m-badge--info m-badge--wide">Versión {{$quote_detail->version}}</span>
                                                                                        </h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->description}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Recurso:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->resource->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Tipo:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->typeTask->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Hora:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->hours}} horas</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Total Costo:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->cost}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Porcentaje de Margen:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->gain_percent}}%</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Margen:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->gain}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Precio de Venta:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->total}}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Modal Edit-->
                                                            <div class="modal fade show" role="document" id="editmodalint{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="m-form" action="{{ route('quotes_details.update',[$quote_detail])}}" method="post" role="form">
                                                                                @csrf @method('patch')
                                                                                <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                                    <label for="name"> Nombre:</label>
                                                                                    <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" value="{{$quote_detail->name}}"
                                                                                        name="name" placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                                    <label class="col-sm-2 control-label" for="description">Descripción:</label>
                                                                                    <div class="col-sm-10">
                                                                                        <textarea type="description" class="form-control" id="description" value="" placeholder="Ingrese descripción" name="description"
                                                                                            cols="30" rows="5">{{$quote_detail->description}}</textarea>
                                                                                        @if($errors->has('description'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('description')
                                                                                            }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('type_id')) has-danger @endif">
                                                                                    <label for="type_id">Tipo:</label>
                                                                                    <select class="form-control selectpicker col-sm-5" name="type_id" id="type_id">
                                                                                        <option value="">Seleccionar proveedor</option>
                                                                                        @foreach($types as $type)
                                                                                        <option value="{{$type->id}}" @if($type->id === $quote_detail->type_id)selected @endif>{{$type->name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @if($errors->has('type_id'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('type_id') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <input type="text" hidden name='type' value='intern'>
                                                                                <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                                                                                    <label class="col-sm-2 control-label" for="resource_id">Recurso:</label>
                                                                                    <select class="form-control m-input m-input--square col-sm-5" name="resource_id" id="resource_id_update">
                                                                                        <option value="">Recursos</option>
                                                                                        @foreach($resources as $resource)
                                                                                        <option value="{{$resource->id}}" @if($resource->id === $quote_detail->resource_id)
                                                                                            selected @endif>{{$resource->name}}
                                                                                        </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @if($errors->has('resource_id'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('resource_id') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('hours')) has-danger @endif">
                                                                                    <label for="hours"> Horas de Trabajo:</label>
                                                                                    <input id="hour_update" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$quote_detail->hours}}"
                                                                                        name="hours" placeholder="Ingrese hora"> @if($errors->has('hours'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('hours') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group">
                                                                                    <label for="cost"> Costo:</label>
                                                                                    <span id='cost_update' name="cost" class="m-badge m-badge--warning m-badge--wide"></span>
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('gain_percent')) has-danger @endif">
                                                                                    <label for="gain_percent"> Porcentaje de Margen:</label>
                                                                                    <input id="gain_percent_update" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$quote_detail->gain_percent}}"
                                                                                        name="gain_percent" placeholder="Ingrese porcentaje"> @if($errors->has('gain_percent'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('gain_percent') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group">
                                                                                    <label for="gain"> Margen:</label>
                                                                                    <span id='gain_update' name="gain" class="m-badge m-badge--success m-badge--wide"></span>
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('total')) has-danger @endif">
                                                                                    <label for="total"> Total:</label>
                                                                                    <span id="total_update" name="total" class="m-badge m-badge--danger m-badge--wide"></span>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <div class="m-form__actions">
                                                                                        <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                                        <button type="submit" class="btn btn-success">Guardar</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Modal Destroy-->
                                                            <div class="modal fade" id="destroymodalint{{$quote_detail->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog m--align-left" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Detalle de Cotización {{ $quote_detail->name
                                                                                }}
                                                                            </h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            ¿Está seguro que desea eliminar este detalle?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                            <form action="{{ route('quotes_details.destroy', [$quote_detail])}}" method="post">
                                                                                @csrf @method('delete')
                                                                                <button class="btn btn-danger" type="submit">Eliminar</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="rec_extern" role="tabpanel">
                        <div class="m-porlet">
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="m-section col mb-0">
                                        <div class="m-section_content">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title d-flex align-items-center">
                                                        <div class="row col">
                                                            <div class="col-lg-6 d-flex align-items-center">
                                                                <h3 class="m-portlet__head-text"> Ingrese los detalles de la cotización</h3>
                                                            </div>
                                                            <!-- Button trigger create modal extern -->
                                                            <div class="col-lg-6 m--align-right">
                                                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#createModalextern{{$quote->id}}">
                                                                    <i class="m-menu__link-icon flaticon-add"></i>
                                                                    Agregar Detalle Externo
                                                                </button>
                                                                <!-- Modal Create Extern-->
                                                                <div class="modal fade show" role="document" id="createModalextern{{$quote->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                    aria-hidden="true">
                                                                    <div class="modal-dialog modal-lg m--align-left">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">Detalle Externo</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">×</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form class="m-form" action="{{route('quotes_details.store',[$quote])}}" method="post" role="form">
                                                                                    @csrf
                                                                                    <input type="text" hidden name='type' value='extern'>
                                                                                    <div class="form-group m-form__group @if($errors->has('dealer_id')) has-danger @endif">
                                                                                        <label for="dealer_id">Proveedor:</label>
                                                                                        <select class="form-control selectpicker col-sm-5" name="dealer_id" id="dealer_id">
                                                                                            <option value="">Seleccionar proveedor</option>
                                                                                            @foreach($dealers as $dealer)
                                                                                            <option value="{{$dealer->id}}">{{$dealer->name}}
                                                                                            </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                        @if($errors->has('dealer_id'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('dealer_id') }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                                        <label for="name_ext">Nombre de la Tarea:</label>
                                                                                        <input id="name_ext" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="" name="name"
                                                                                            placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('name') }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                                        <label for="description">Descripción:</label>
                                                                                        <div class="col-sm-10">
                                                                                            <textarea type="description" class="form-control m-input" id="description" value="" placeholder="Ingrese descripción" name="description"
                                                                                                cols="30" rows="5"></textarea>
                                                                                            @if($errors->has('description'))
                                                                                            <div class="form-control-feedback">{{ $errors->first('description')
                                                                                                }}
                                                                                            </div>
                                                                                            @endif
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('cost')) has-danger @endif">
                                                                                        <label for="cost"> Costo del Proveedor:</label>
                                                                                        <input id="cost_ext" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="" name="cost"
                                                                                            placeholder="Ingrese costo"> @if($errors->has('cost'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('cost') }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('gain_percent')) has-danger @endif">
                                                                                        <label for="gain_percent"> Porcentaje de Margen:</label>
                                                                                        <input id="gain_percent_ext" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value=""
                                                                                            name="gain_percent" placeholder="Ingrese porcentaje"> @if($errors->has('gain_percent'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('gain_percent')
                                                                                            }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-group m-form__group">
                                                                                        <label> Margen:</label>
                                                                                        <span id='gain_ext' name="gain" class="m-badge m-badge--success m-badge--wide"></span>
                                                                                    </div>
                                                                                    <div class="form-group m-form__group @if($errors->has('total')) has-danger @endif">
                                                                                        <label for="total"> Total:</label>
                                                                                        <span id="total_ext" name="total" class="m-badge m-badge--danger m-badge--wide"></span>
                                                                                    </div>
                                                                                    <div class="modal-footer d-flex mb-0 padding-top">
                                                                                        <div class="m-form__actions padding-top">
                                                                                            <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                                            <button type="submit" class="btn btn-success">Guardar</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered m-table m-table--head-bg-brand">
                                                <thead>
                                                    <tr>
                                                        <th class="m--img-centered">#</th>
                                                        <th class="m--img-centered">Proveedor</th>
                                                        <th class="m--img-centered">Costo de Venta</th>
                                                        <th class="m--img-centered">% Margen</th>
                                                        <th class="m--img-centered">Margen</th>
                                                        <th class="m--img-centered">Precio de Venta</th>
                                                        <th class="m--img-centered">Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($quotes_details_extern as $quote_detail)
                                                    <tr>
                                                        <td class="m--align-center">{{$loop->iteration}}</td>
                                                        <td class="m--align-center">{{$quote_detail->dealer->name}}</td>
                                                        <td class="m--align-center">$ {{$quote_detail->cost}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain_percent}} %</td>
                                                        <td class="m--align-center">$ {{$quote_detail->gain}}</td>
                                                        <td class="m--align-center">$ {{$quote_detail->total}}</td>
                                                        <td class="m--align-center">
                                                            <!-- Button trigger show modal -->
                                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showmodalitemext{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-visible"></i>
                                                                Ver
                                                            </button>

                                                            <!-- Button trigger edit modal -->
                                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editmodalitemext{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-edit"></i>
                                                                Editar
                                                            </button>

                                                            <!-- Button trigger modal destroy -->
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#destroymodalitemext{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-circle"></i>
                                                                Eliminar
                                                            </button>

                                                            <!-- Modal Show-->
                                                            <div class="modal fade show" role="document" id="showmodalitemext{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre del Proveedor:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->dealer->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre de la  Tarea:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Descripción:
                                                                                            <span class="m-badge m-badge--info m-badge--wide">Versión {{$quote_detail->version}}</span>
                                                                                        </h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->description}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Total Costo:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->cost}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Porcentaje de Margen:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->gain_percent}}%</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Margen:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->gain}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Precio de Venta:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->total}}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- Modal Edit-->
                                                            <div class="modal fade show" role="document" id="editmodalitemext{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="m-form" action="{{ route('quotes_details.update',[$quote_detail])}}" method="post" role="form">
                                                                                @csrf @method('patch')
                                                                                <input type="text" hidden name='type' value='extern'>
                                                                                <div class="form-group m-form__group @if($errors->has('dealer_id')) has-danger @endif">
                                                                                    <label for="dealer_id">Proveedor:</label>
                                                                                    <select class="form-control selectpicker col-sm-5" name="dealer_id" id="dealer_id">
                                                                                        <option value="">Seleccionar proveedor</option>
                                                                                        @foreach($dealers as $dealer)
                                                                                        <option value="{{$dealer->id}}" @if($dealer->id === $quote_detail->dealer_id)selected
                                                                                            @endif>{{$dealer->name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @if($errors->has('dealer_id'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('dealer_id') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                                    <label for="name">Nombre de la Tarea:</label>
                                                                                    <input id="name" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$quote_detail->name}}"
                                                                                        name="name" placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('name') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                                    <label for="description">Descripción:</label>
                                                                                    <div class="col-sm-10">
                                                                                        <textarea type="description" class="form-control m-input" id="description" value="" placeholder="Ingrese descripción" name="description"
                                                                                            cols="30" rows="5">{{$quote_detail->description}}</textarea>
                                                                                        @if($errors->has('description'))
                                                                                        <div class="form-control-feedback">{{ $errors->first('description')
                                                                                            }}
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('cost')) has-danger @endif">
                                                                                    <label for="cost"> Costo del Proveedor:</label>
                                                                                    <input id="cost_ext_upt" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$quote_detail->cost}}"
                                                                                        name="cost" placeholder="Ingrese costo"> @if($errors->has('cost'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('cost') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('gain_percent')) has-danger @endif">
                                                                                    <label for="gain_percent"> Porcentaje de Margen:</label>
                                                                                    <input id="gain_percent_ext_upt" type="text" class="form-control form-control-danger m-input m-input--square col-lg-3" value="{{$quote_detail->gain_percent}}"
                                                                                        name="gain_percent" placeholder="Ingrese porcentaje"> @if($errors->has('gain_percent'))
                                                                                    <div class="form-control-feedback">{{ $errors->first('gain_percent') }}
                                                                                    </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="form-group m-form__group">
                                                                                    <label> Margen:</label>
                                                                                    <span id='gain_ext_upt' name="gain" class="m-badge m-badge--success m-badge--wide"></span>
                                                                                </div>
                                                                                <div class="form-group m-form__group @if($errors->has('total')) has-danger @endif">
                                                                                    <label for="total"> Total:</label>
                                                                                    <span id="total_ext_upt" name="total" class="m-badge m-badge--danger m-badge--wide"></span>
                                                                                </div>
                                                                                <div class="modal-footer d-flex mb-0 padding-top">
                                                                                    <div class="m-form__actions padding-top">
                                                                                        <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                                        <button type="submit" class="btn btn-success">Guardar</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <!-- Modal Destroy-->
                                                            <div class="modal fade" id="destroymodalitemext{{$quote_detail->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog m--align-left" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Detalle de Cotización {{ $quote_detail->name
                                                                                }}
                                                                            </h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            ¿Está seguro que desea eliminar este detalle?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                            <form action="{{ route('quotes_details.destroy', [$quote_detail])}}" method="post">
                                                                                @csrf @method('delete')
                                                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('scripts')
<script>
    $('.date').datepicker({
        orientation: "bottom left",
        autoclose: true,
        format: "yyyy-mm-dd",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    $('.select').select2({
        placeholder: "Seleccionar cliente"
    });
    $('#resource_id').select2({
        placeholder: "Seleccionar recurso"
    });
    $('status_id').select2({
        placeholder: "Seleccionar estado"
    });
    $('type_id_ext').select2({
        placeholder: "Seleccionar tipo"
    });
    $('.summernote').summernote({
        height: 140
    });
</script>
<script>
    const number = document.getElementById('hour_work')
    const gain = document.getElementById('gain')
    const gain_percent = document.getElementById('gain_percent')
    const cost = document.getElementById('cost')
    const total = document.getElementById('total_label')
    let value = 0
    let price = 0;

    $('#resource_id').on('change', function () {
        let route = `/api/resource/${this.value}`;
        $.ajax({
            url: route,
        }).done(function (response) {
            price = response.price;
        });
    })
    let newValue = 0
    cost.innerText = value;
    number.addEventListener("blur", function () {
        newValue = number.value;
        newValue = Math.round((newValue * price) * 100) / 100;
        cost.innerText = newValue;
    });

    gain.innerText = value
    total.innerText = newValue

    gain_percent.addEventListener("blur", function () {
        let newValues = gain_percent.value
        newgain = Math.round(((newValues * newValue) / 100) * 100) / 100;
        gain.innerText = newgain;
        total.innerText = newgain + newValue
    });
</script>
<script>
    const hour = document.getElementById('hour_update')
    const gain_upd = document.getElementById('gain_update')
    const gain_percent_upd = document.getElementById('gain_percent_update')
    const cost_upd = document.getElementById('cost_update')
    const total_upd = document.getElementById('total_update')
    let value_upd = 0
    let price_upd = 0;

    $('#resource_id_update').on('change', function () {
        let route = `/api/resource/${this.value}`;
        $.ajax({
            url: route,
        }).done(function (response) {
            price_upd = response.price;
        });
    })
    let tmp3 = 0
    cost_upd.innerText = value_upd;
    hour.addEventListener("blur", function () {
        tmp3 = hour.value;
        tmp3 = Math.round((tmp3 * price_upd) * 100) / 100;
        cost_upd.innerText = tmp3;
    });

    gain_upd.innerText = value_upd
    total_upd.innerText = tmp3

    gain_percent_upd.addEventListener("blur", function () {
        let tmp4 = gain_percent_upd.value
        tmp4 = Math.round((tmp4 * tmp3) / 100) * 100) / 100; gain_upd.innerText = tmp4; total_upd.innerText =
    tmp4 + tmp3
    });
</script>
<script>
    const gain_ext = document.getElementById('gain_ext')
    const gain_percent_ext = document.getElementById('gain_percent_ext')
    const cost_ext = document.getElementById('cost_ext')
    const total_ext = document.getElementById('total_ext')
    let tmp = 0
    let costn = 0

    cost_ext.addEventListener("blur", function () {
        costn = Math.round(cost_ext.value * 100) / 100;
    });

    gain_ext.innerText = tmp
    total_ext.innerText = tmp

    gain_percent_ext.addEventListener("blur", function () {
        let tmp2 = gain_percent_ext.value
        tmp2 = Math.round(((tmp2 * costn) / 100) * 100) / 100;
        gain_ext.innerText = tmp2;
        total_ext.innerText = parseFloat(tmp2) + parseFloat(costn)
    });
</script>
<script>
    const gain_ext_upt = document.getElementById('gain_ext_upt')
    const gain_percent_ext_upt = document.getElementById('gain_percent_ext_upt')
    const cost_ext_upt = document.getElementById('cost_ext_upt')
    const total_ext_upt = document.getElementById('total_ext_upt')
    let tmp5 = 0
    let costnew = 0

    cost_ext_upt.addEventListener("blur", function () {
        costnew = Math.round(cost_ext_upt.value * 100) / 100;
    });

    gain_ext_upt.innerText = tmp5
    total_ext_upt.innerText = tmp5

    gain_percent_ext_upt.addEventListener("blur", function () {
        let tmp6 = gain_percent_ext_upt.value
        tmp6 = Math.round(((tmp6 * costnew) / 100) * 100) / 100;
        gain_ext_upt.innerText = tmp6;
        total_ext_upt.innerText = Math.round((parseFloat(tmp6) + parseFloat(costnew)) * 100) / 100
    });
</script>
@endpush