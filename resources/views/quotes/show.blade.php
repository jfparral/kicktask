@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Crear Cotización'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('quotes.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Cotizaciones</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('quotes.show',[$quote])}}" class="m-nav__link">
        <span class="m-nav__link-text">Ver</span>
    </a>
</li>
@slot('options')
<a href="{{route('quotes.index')}}" class="btn btn-metal">Regresar</a>
@endslot @endbreadcrumbs
<div class="m-content">
    <div class="m-porlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Datos Generales de la Cotización</h3>
                    </div>
                </div>
            </div>
            <div class="m-porlet__body">
                <!-- General Data: Quote -->
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <div class="row">
                                            <div class="col-lg-1 m--align-center">
                                                <img src="{{$quote->client->logo}}" class="-m--img-rounded m--img-centered" alt="" height="50">
                                            </div>
                                            <div class="col-lg-6 d-flex align-items-center">
                                                <h3 class="mb-0">{{$quote->client->name}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Contacto:</h3>
                                        <span class="m--font-boldest">{{$quote->contact->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Términos Legales:</h3>
                                        <span class="m--font-boldest">{!!$quote->terms!!}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Estado:</h3>
                                        <span class="m--font-boldest">{{$quote->status->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Fecha de Expiración:</h3>
                                        <span class="m--font-boldest">{{$quote->exp_date}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Forma de Pago:</h3>
                                        <span class="m--font-boldest">{{$quote->payment}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-porlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Detalles de la Cotización</h3>
                    </div>
                </div>
            </div>
            <div class="m-porlet__body">
                <!-- Tabs Noucht-->
                <ul class="nav nav-pills nav-fill" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rec_intern">Recursos Internos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#rec_extern">Recursos Externos</a>
                    </li>
                </ul>
                <!-- Tabs Body-->
                <div class="tab-content">
                    <div class="tab-pane" id="rec_intern" role="tabpanel">
                        <div class="m-porlet">
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="m-section col mb-0">
                                        <div class="m-section_content">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title d-flex align-items-center">
                                                        <div class="row col">
                                                            <div class="col-lg-6 d-flex align-items-center">
                                                                <h3 class="m-portlet__head-text"> Ingrese los detalles de la cotización</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered m-table m-table--head-bg-brand">
                                                <thead>
                                                    <tr>
                                                        <th class="m--img-centered">#</th>
                                                        <th class="m--img-centered">Nombre</th>
                                                        <th class="m--img-centered">Recurso</th>
                                                        <th class="m--img-centered">Costo de Venta</th>
                                                        <th class="m--img-centered">% Margen</th>
                                                        <th class="m--img-centered">Margen</th>
                                                        <th class="m--img-centered">Precio de Venta</th>
                                                        <th class="m--img-centered">Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($quotes_details_intern as $quote_detail)
                                                    <tr>
                                                        <td class="m--align-center">{{$loop->iteration}}</td>
                                                        <td class="m--align-center">{{$quote_detail->name}}</td>
                                                        <td class="m--align-center">{{$quote_detail->resource->name}}</td>
                                                        <td class="m--align-center">{{$quote_detail->cost}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain_percent}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain}}</td>
                                                        <td class="m--align-center">{{$quote_detail->total}}</td>
                                                        <td class="m--align-center">
                                                            <!-- Button trigger show modal -->
                                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showmodalint{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-visible"></i>
                                                                Ver
                                                            </button>

                                                            <!-- Modal Show-->
                                                            <div class="modal fade show" role="document" id="showmodalint{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->name}}"</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Descripción:
                                                                                            <span class="m-badge m-badge--info m-badge--wide">Versión {{$quote_detail->version}}</span>
                                                                                        </h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->description}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Recurso:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->resource->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Hora:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->hours}} horas</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Total Costo:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->cost}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Porcentaje de Margen:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->gain_percent}}%</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Margen:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->gain}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Precio de Venta:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->total}}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="rec_extern" role="tabpanel">
                        <div class="m-porlet">
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="m-section col mb-0">
                                        <div class="m-section_content">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title d-flex align-items-center">
                                                        <div class="row col">
                                                            <div class="col-lg-6 d-flex align-items-center">
                                                                <h3 class="m-portlet__head-text"> Ingrese los detalles de la cotización</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-bordered m-table m-table--head-bg-brand">
                                                <thead>
                                                    <tr>
                                                        <th class="m--img-centered">#</th>
                                                        <th class="m--img-centered">Proveedor</th>
                                                        <th class="m--img-centered">Costo de Venta</th>
                                                        <th class="m--img-centered">% Margen</th>
                                                        <th class="m--img-centered">Margen</th>
                                                        <th class="m--img-centered">Precio de Venta</th>
                                                        <th class="m--img-centered">Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($quotes_details_extern as $quote_detail)
                                                    <tr>
                                                        <td class="m--align-center">{{$loop->iteration}}</td>
                                                        <td class="m--align-center">{{$quote_detail->dealer->name}}</td>
                                                        <td class="m--align-center">{{$quote_detail->cost}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain_percent}}</td>
                                                        <td class="m--align-center">{{$quote_detail->gain}}</td>
                                                        <td class="m--align-center">{{$quote_detail->total}}</td>
                                                        <td class="m--align-center">
                                                            <!-- Button trigger show modal -->
                                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showmodalitemext{{$quote_detail->id}}">
                                                                <i class="m-menu__link-icon flaticon-visible"></i>
                                                                Ver
                                                            </button>

                                                            <!-- Modal Show-->
                                                            <div class="modal fade show" role="document" id="showmodalitemext{{$quote_detail->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                                aria-hidden="true">
                                                                <div class="modal-dialog modal-lg m--align-left">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Detalles</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre del Proveedor:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->dealer->name}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Nombre de Tarea:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->name}}"</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Descripción:
                                                                                            <span class="m-badge m-badge--info m-badge--wide">Versión {{$quote_detail->version}}</span>
                                                                                        </h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->description}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Total Costo:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->cost}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Porcentaje de Margen:</h3>
                                                                                        <span class="m--font-boldest">{{$quote_detail->gain_percent}}%</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Margen:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->gain}}</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="m-stack__item">
                                                                                    <div class="m-stack__demo-item">
                                                                                        <h3 class="m-section__heading">Precio de Venta:</h3>
                                                                                        <span class="m--font-boldest">$ {{$quote_detail->total}}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection