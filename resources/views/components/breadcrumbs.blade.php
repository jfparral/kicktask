<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">{{ $title }}</h3>			
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                {{ $slot }}
            </ul>
        </div>
        <div>
            {{ $options }}
        </div>
    </div>
</div>