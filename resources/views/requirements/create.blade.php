@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Crear Solicitud'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('requirements.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Solicitudes</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('requirements.create')}}" class="m-nav__link">
        <span class="m-nav__link-text">Crear</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section_content">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text"> Ingrese los detalles de la solicitud</h3>
                                </div>
                            </div>
                        </div>
                        <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="post" action="{{route('requirements.store')}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group m-form__group @if($errors->has('client_id')) has-danger @endif">
                                <label for="client_id">Clientes:</label>
                                <select class="form-control selectpicker col-lg-3" name="client_id" id="client_id">
                                    <option value="">Seleccionar cliente</option>
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}">{{$client->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('client_id'))
                                    <div class="form-control-feedback">{{ $errors->first('client_id') }}</div>
                                @endif
                            </div>
                            <div class="form-group m-form__group @if($errors->has('contact_id')) has-danger @endif">
                                <label for="contact_id">Contactos:</label>
                                <select class="form-control selectpicker col-lg-3" name="contact_id" id="contact_id">
                                    <option value="">Seleccionar contacto</option>
                                    @foreach($contacts as $contact)
                                        <option value="{{$contact->id}}">{{$contact->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('contact_id'))
                                    <div class="form-control-feedback">{{ $errors->first('contact_id') }}</div>
                                @endif
                            </div>
                            <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                <label for="description">Descripción:</label>
                                <textarea class="form-control m-input summernote" name="description" id="description" cols="80" rows="5" placeholder="Ingrese descripción"></textarea>
                                @if($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                            <div class="form-group m-form__group @if($errors->has('due_date')) has-danger @endif">
                                <label for="date"> Fecha de Entrega:</label>
                                <input name="due_date" class="form-control date col-lg-3" type="date" value="" id="date" readonly>
                                @if($errors->has('due_date'))
                                <div class="form-control-feedback">{{ $errors->first('due_date') }}</div>
                                @endif
                            </div>
                            <div class="form-group m-form__group @if($errors->has('source_id')) has-danger @endif">
                                <label for="source_id">Fuente:</label>
                                <select class="form-control selectpicker col-lg-3" name="source_id" id="source_id">
                                    <option value="">Seleccionar fuente</option>
                                    @foreach($sources as $source)
                                        <option value="{{$source->id}}">{{$source->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('source_id'))
                                    <div class="form-control-feedback">{{ $errors->first('source_id') }}</div>
                                @endif
                            </div>
                            <div class="m-form__actions">
                                <a href="{{ route("requirements.index")}}" class="btn btn-metal">Cancelar</a>
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('.date').datepicker({
            orientation: "bottom left",
            autoclose: true,
            format: "yyyy-mm-dd",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('.select').select2({
            placeholder: "Seleccionar cliente"
        });
        $('.summernote').summernote({
            height: 200
        });
    </script>
@endpush