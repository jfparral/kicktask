@extends('layouts.layout') 

@section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Solicitudes'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('requirements.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Solicitudes</span>
    </a>
</li>
@slot('options')
<a href="{{ route('requirements.create') }}" class="btn btn-primary">Agregar solicitud</a>
@endslot @endbreadcrumbs
<!-- END: Subheader -->

<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="m-section col mb-0">
                    <div class="m-section_content">
                        <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="GET" action="{{ route('requirements.index') }}">
                            <div class="row d-flex p-4">
                                <div class="col-lg-3">
                                    <label for="client_id">Cliente:</label>
                                    <select class="form-control selectpicker" name="client_id" id="client_id">
                                        <option value=""> Seleccionar cliente</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div> 
                                <div class="col-lg-3">
                                    <label for="source_id">Fuente:</label>
                                    <select class="form-control selectpicker" name="source_id" id="source_id">
                                        <option value=""> Seleccionar fuente</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->name}}</option>
                                        @endforeach
                                    </select>
                                </div>    
                                <div class="col-lg-2 align-items-center">
                                    <label for="from">Desde:</label>
                                    <input type="text" class="form-control date" name="from" id="from" readonly placeholder="Fecha desde"/>
                                </div>
                                <div class="col-lg-2 align-items-center">
                                    <label for="to">Hasta:</label>
                                    <input type="text" class="form-control date" name="to" id="to" readonly placeholder="Fecha hasta"/>
                                </div>
                                <div class="col-lg-2 d-flex align-items-end">
                                    <button class="btn btn-success">Filtrar</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-bordered m-table m-table--head-bg-brand mb-0">
                            <thead>
                                <tr>
                                    <th class="m--img-centered">#</th>
                                    <th class="m--img-centered">Cliente</th>
                                    <th class="m--img-centered">Estado</th>
                                    <th class="m--img-centered">Fecha de Entrega</th>
                                    <th class="m--img-centered">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($requirements as $requirement)
                                <tr>
                                    <td class="m--img-centered" >{{$loop->iteration}}</td>
                                    <td class="m--img-centered">{{$requirement->client->name}}</td>
                                    <td class="m--img-centered">
                                        <span class="m-badge m-badge--success m-badge--wide">{{$requirement->status->name}}</span>
                                    </td>
                                    <td class="m--img-centered">{{$requirement->due_date}}</td>
                                    <td class="m--img-centered">
                                        <a href="{{ route('requirements.show', [$requirement])}}" class="btn btn-success btn-sm">
                                            <i class="m-menu__link-icon flaticon-visible"></i>
                                            Ver
                                        </a>
                                        <a href="{{ route('requirements.edit', [$requirement])}}" class="btn btn-info btn-sm">
                                            <i class="m-menu__link-icon flaticon-edit"></i>
                                            Editar
                                        </a>

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$requirement->id}}">
                                            <i class="m-menu__link-icon flaticon-circle"></i>
                                            Eliminar
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="eliminarModal{{$requirement->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Solicitud de {{ $requirement->client->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        ¿Está seguro que desea eliminar esta solicitud?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        <form action="{{ route('requirements.destroy', [$requirement])}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-primary">
                                                                Eliminar
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $('.date').datepicker({
            orientation: "bottom left",
            autoclose: true,
            format: "yyyy-mm-dd",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#client_id').select2({
            placeholder: "Seleccionar cliente"
        });
        $('#source_id').select2({
            placeholder:"Selecionar fuente"
        });
    </script>
@endpush