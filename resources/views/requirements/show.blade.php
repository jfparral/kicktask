@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Detalles de la Solicitud'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('requirements.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Solicitudes</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('requirements.show',[$requirement])}}" class="m-nav__link">
        <span class="m-nav__link-text">Ver</span>
    </a>
</li>
@slot('options')
<a href="#" class="btn btn-success">
    <i class="m-menu__link-icon flaticon-multimedia-1"></i>
</a>
<a href="{{ route('requirements.index') }}" class="btn btn-metal">Regresar</a>
@endslot @endbreadcrumbs
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div class="m-portlet__body mb-0">
                <div class="m-section">
                    <div class="m-section__content">
                        <div class="m-demo mb-0" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Cliente:</h3>
                                        <span class="m--font-boldest">{{$requirement->client->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Contacto:</h3>
                                        <span class="m--font-boldest">{{$requirement->contact->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Descripción:</h3>
                                        <span class="m--font-boldest">{!! $requirement->description !!}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Fecha de Entrega:</h3>
                                        <span class="m--font-boldest">{{$requirement->due_date}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Estado:</h3>
                                        <span class="m--font-boldest">{{$requirement->status->name}}</span>
                                    </div>
                                </div>
                                <div class="m-stack__item">
                                    <div class="m-stack__demo-item">
                                        <h3 class="m-section__heading">Fuente:</h3>
                                        <span class="m--font-boldest">{{$requirement->source->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-content pt-0">
    <div class="m-portlet m-portlet--tab">
        <div class="mportlet-head">
            <div>
                <div class="m-section align-items-center">
                    <div class="m-section__content mb-0">
                        <div class="m-portlet__head mb-0 m--align-left d-flex align-items-center">
                            <div class="row col">
                                <div class="col-lg-7">
                                    <h3 class="m-porlet__head__tittle mb-0"> Tareas</h3>
                                </div>
                                <div class="col-lg-5 m--align-right">
                                    <div class="row col">
                                        <div class="col-lg-8">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                Agregar Tarea
                                            </button>

                                            <!-- Modal Agregar Tarea -->
                                            <div class="modal fade bd-example-modal-lg m--align-left" id=".bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="bd-example-modal-lg"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title">{{$requirement->client->name}}: Agregar Tarea</h3>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="m-form" action="{{ route('tasks.store',[$requirement])}}" method="post" role="form">
                                                                @csrf
                                                                <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                    <label class="col-sm-2" for="name">Nombre</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese nombre" /> @if($errors->has('name'))
                                                                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group @if($errors->has('due_date')) has-danger @endif">
                                                                    <label class="col-sm-2" for="date"> Fecha:</label>
                                                                    <input name="due_date" class="form-control date col-lg-3" readonly type="date" value="" id="date"> @if($errors->has('due_date'))
                                                                    <div class="form-control-feedback">{{ $errors->first('due_date') }}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                    <label class="col-sm-2" for="description">Descripción</label>
                                                                    <div class="col-sm-10">
                                                                        <textarea type="text" class="form-control" id="description" placeholder="Ingrese descripción" name="description"
                                                                            cols="30" rows="5"></textarea>
                                                                        @if($errors->has('description'))
                                                                        <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group @if($errors->has('type_id')) has-danger @endif">
                                                                    <label class="col-sm-2" for="type_id">Tipo:</label>
                                                                    <select class="form-control m-input m-input--square col-sm-5" name="type_id" id="type_id">
                                                                        <option value="">Seleccionar tipo</option>
                                                                        @foreach($types as $type)
                                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if($errors->has('type_id'))
                                                                    <div class="form-control-feedback">{{ $errors->first('type_id') }}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                                                                    <label class="col-sm-2" for="resource_id">Recurso:</label>
                                                                    <select class="form-control m-input m-input--square col-sm-5" name="resource_id" id="resource_id">
                                                                        <option value="">Recursos</option>
                                                                        @foreach($resources as $resource)
                                                                        <option value="{{$resource->id}}">{{$resource->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if($errors->has('resource_id'))
                                                                    <div class="form-control-feedback">{{ $errors->first('resource_id') }}</div>
                                                                    @endif
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <div class="m-form__actions">
                                                                        <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                        <button type="submit" class="btn btn-success">Guardar</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <!-- Create Cotización -->
                                            <form class="m-form " method="post" action="{{route('quotes.store',[$requirement])}}">
                                                @csrf
                                                <input type="text" hidden name="client_name" id="client_name" value="{{$requirement->client->name}}">
                                                <input type="text" hidden name="comercial_name" id="comercial_name" value="{{$requirement->client->comercial_name}}">
                                                <input type="email" hidden name="client_email" id="client_email" value="{{$requirement->client->email}}">
                                                <input type="text" hidden name="client_phone" id="client_phone" value="{{$requirement->client->phone}}">
                                                <input type="text" hidden name="client_address" id="client_address" value="{{$requirement->client->address}}">
                                                <input type="text" hidden name="client_legal_id" id="client_legal_id" value="{{$requirement->client->legal_id}}">
                                                <input type="text" hidden name="contact_name" id="contact_name" value="{{$requirement->contact->name}}">
                                                <input type="text" hidden name="contact_email" id="contact_email" value="{{$requirement->contact->email}}">
                                                <input type="text" hidden name="contact_id" id="contact_id" value="{{$requirement->contact->id}}">
                                                <input type="text" hidden name="client_id" id="client_id" value="{{$requirement->client->id}}">
                                                <input type="text" hidden name="user_id" id="user_id" value="{{$requirement->user->id}}">
                                                <input type="text" hidden name="requirement_id" id="requirement_id" value="{{$requirement->id}}">
                                                <button class="btn btn-success btn-sm" type="submit">
                                                    Agregar Cotización
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body mb-0 pt-0">
                        <div class="m-section col mb-0">
                            <div class="m-section__content">
                                <table class="table table-bordered m-table m-table--head-bg-brand">
                                    <thead>
                                        <tr>
                                            <th class="m--img-centered">#</th>
                                            <th class="m--img-centered">Nombre</th>
                                            <th class="m--img-centered">Recurso</th>
                                            <th class="m--img-centered">Tipo</th>
                                            <th class="m--img-centered">Fecha de Entrega</th>
                                            <th class="m--img-centered">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($requirement->tasks as $task)
                                        <tr>
                                            <td class="m--img-centered"> {{$loop->iteration}} </td>
                                            <td class="m--img-centered">{{$task->status->name}}</td>
                                            <td class="m--img-centered">{{$task->resource->name}}</td>
                                            <td class="m--img-centered">{{$task->type->name}}</td>
                                            <td class="m--img-centered">{{$task->due_date}}</td>
                                            <td class="m--img-centered">
                                                <!-- Button trigger show modal -->
                                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#showModal{{$task->id}}">
                                                    <i class="m-menu__link-icon flaticon-visible"></i>
                                                    Ver
                                                </button>

                                                <!-- Button trigger edit modal -->
                                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editModal{{$task->id}}">
                                                    <i class="m-menu__link-icon flaticon-edit"></i>
                                                    Editar
                                                </button>

                                                <!-- Button trigger modal destroy -->
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#destroyModal{{$task->id}}">
                                                    <i class="m-menu__link-icon flaticon-circle"></i>
                                                    Eliminar
                                                </button>

                                                <!-- Modal Show-->
                                                <div class="modal fade show" role="document" id="showModal{{$task->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-lg m--align-left">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Detalles de la Tarea</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="height: 150px">
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Nombre:</h3>
                                                                            <span class="m--font-boldest">{{$task->name}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Descripción:
                                                                                <span class="m-badge m-badge--info m-badge--wide">Versión {{$task->lastVersion()->version}}</span>
                                                                            </h3>
                                                                            <span class="m--font-boldest">{!!$task->lastVersion()->description!!}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Fecha de Entrega:</h3>
                                                                            <span class="m--font-boldest">{{$task->due_date}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Área de Recurso:</h3>
                                                                            <span class="m--font-boldest">{{$task->resource->name}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Estado:</h3>
                                                                            <span class="m--font-boldest">{{$task->status->name}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-stack__item">
                                                                        <div class="m-stack__demo-item">
                                                                            <h3 class="m-section__heading">Tipo:</h3>
                                                                            <span class="m--font-boldest">{{$task->type->name}}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Modal Edit-->
                                                <div class="modal fade show" role="document" id="editModal{{$task->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-lg m--align-left">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Detalles de la Tarea</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="m-form form-control" action="{{ route('tasks.update',[$task])}}" method="post" role="form">
                                                                    @csrf @method('PATCH')
                                                                    <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                                                                        <label for="name"> Nombre:</label>
                                                                        <input id="name" type="text" class="form-control form-control-danger m-input m-input--square" value="{{$task->name}}" name="name"
                                                                            placeholder="Ingrese nombre"> @if($errors->has('name'))
                                                                        <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group m-form__group @if($errors->has('due_date')) has-danger @endif">
                                                                        <label class="col-sm-2 control-label" for="date"> Fecha:</label>
                                                                        <input name="due_date" class="form-control date col-lg-3" readonly type="date" value="{{$task->due_date}}" placeholder="Ingrese nombre"
                                                                            id="date"> @if($errors->has('due_date'))
                                                                        <div class="form-control-feedback">{{ $errors->first('due_date') }}</div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group m-form__group @if($errors->has('description')) has-danger @endif">
                                                                        <label class="col-sm-2 control-label" for="description">Descripción</label>
                                                                        <div class="col-sm-10">
                                                                            <textarea type="description" class="form-control m-input summernote col-lg-8" id="description" value="" placeholder="Ingrese descripción"
                                                                                name="description" cols="30" rows="5">{!!$task->lastVersion()->description!!}</textarea>
                                                                            @if($errors->has('description'))
                                                                            <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group m-form__group @if($errors->has('type_id')) has-danger @endif">
                                                                        <label class="col-sm-2 control-label" for="type_id">Tipo:</label>
                                                                        <select class="form-control selectpicker col-sm-5" name="type_id" id="type_id">
                                                                            <option value="">Seleccionar tipo</option>
                                                                            @foreach($types as $type)
                                                                            <option value="{{$type->id}}" @if($type->id === $task->type_id) selected @endif>{{$type->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if($errors->has('type_id'))
                                                                        <div class="form-control-feedback">{{ $errors->first('type_id') }}</div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group m-form__group @if($errors->has('status_id')) has-danger @endif">
                                                                        <label class="col-sm-2 control-label" for="status_id">Estado:</label>
                                                                        <select class="form-control selectpicker col-sm-5" name="status_id" id="status_id">
                                                                            <option value="">Seleccionar estado</option>
                                                                            @foreach($statuses as $status)
                                                                            <option value="{{$status->id}}" @if($status->id === $task->status_id) selected @endif>{{$status->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if($errors->has('status_id'))
                                                                        <div class="form-control-feedback">{{ $errors->first('status_id') }}</div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group m-form__group @if($errors->has('resource_id')) has-danger @endif">
                                                                        <label class="col-sm-2 control-label" for="resource_id">Recurso:</label>
                                                                        <select class="form-control selectpicker col-sm-5" name="resource_id" id="resource_id">
                                                                            <option value="">Seleccionar recurso</option>
                                                                            @foreach($resources as $resource)
                                                                            <option value="{{$resource->id}}" @if($resource->id === $task->resource_id) selected @endif>{{$resource->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if($errors->has('resource_id'))
                                                                        <div class="form-control-feedback">{{ $errors->first('resource_id') }}</div>
                                                                        @endif
                                                                    </div>
                                                                    <input name='link' type="text" hidden="true" value="requirement">
                                                                    <div class="modal-footer">
                                                                        <div class="m-form__actions">
                                                                            <button type="button" class="btn btn-metal" data-dismiss="modal">Cancelar</button>
                                                                            <button type="submit" class="btn btn-success">Guardar</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <!-- Modal Destroy-->
                                                <div class="modal fade" id="destroyModal{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog m--align-left" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Eliminar Tarea {{ $task->name }}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                ¿Está seguro que desea eliminar esta Tarea?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                <a class="btn btn-primary" href="{{ route('services.destroy', [$task])}}">Eliminar </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('scripts')
<script>
    $('.date').datepicker({
        orientation: "bottom left",
        autoclose: true,
        format: "yyyy-mm-dd",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
    $('.select').select2({
        placeholder: "Seleccionar cliente"
    });
    $('#resource_id').select2({
        placeholder: "Seleccionar recurso"
    });
    $('#type_id').select2({
        placeholder: "Seleccionar tipo"
    });
    $('#status_id').select2({
        placeholder: "Seleccionar estado"
    });
    $('.summernote').summernote({
        height: 140
    });
</script>
@endpush