@extends('layouts.layout') 

@section('content')
    <!-- BEGIN: Subheader -->
    @breadcrumbs(['title' => 'Tipos'])
        <li class="m-nav__item m-nav__item--home">
            <a href="/" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i>
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="" class="m-nav__link">
                <span class="m-nav__link-text">Tipos</span>
            </a>
        </li>
        @slot('options')
            <a href="{{ route('types.create') }}" class="btn btn-primary">Agregar tipo</a>
        @endslot
    @endbreadcrumbs
    <!-- END: Subheader -->

    <div class="m-content">
        <!--Section-->
        <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="m-section col mb-0">
                        <div class="m-section_content">
                            <table class="table table-bordered m-table m-table--head-bg-brand mb-0">
                                <thead>
                                    <tr>
                                        <th class="m--img-centered">#</th>
                                        <th class="m--img-centered">Nombre</th>
                                        <th class="m--img-centered">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($types as $type)
                                    <tr>
                                        <td class="m--img-centered" >{{$loop->iteration}}</td>
                                        <td class="m--img-centered">{{$type->name}}</td>
                                        <td class="m--img-centered">
                                            <a href="{{ route('types.edit', [ 'id'=>$type->id]) }}">
                                                <button type="button" class="btn btn-info btn-sm">Editar</button>
                                            </a>

                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#eliminarModal{{$type->id}}">Eliminar</button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="eliminarModal{{$type->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar Tipo {{ $type->name }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            ¿Está seguro que desea eliminar este tipo?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                            <form action="{{ route('types.destroy', [$type]) }}" method="POST">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger">
                                                                    <i class="la la-trash"></i> Eliminar
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection