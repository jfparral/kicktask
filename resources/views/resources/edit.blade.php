@extends('layouts.layout') @section('content')
<!-- BEGIN: Subheader -->
@breadcrumbs(['title' => 'Actualizar Recurso'])
<li class="m-nav__item m-nav__item--home">
    <a href="/" class="m-nav__link m-nav__link--icon">
        <i class="m-nav__link-icon la la-home"></i>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('resources.index')}}" class="m-nav__link">
        <span class="m-nav__link-text">Recursos</span>
    </a>
</li>
<li class="m-nav__separator">-</li>
<li class="m-nav__item">
    <a href="{{route('resources.edit',[$resource])}}" class="m-nav__link">
        <span class="m-nav__link-text">Editar</span>
    </a>
</li>
@slot('options') @endslot @endbreadcrumbs
<!-- END: Subheader -->
<div class="m-content">
    <!--Section-->
    <div class="m-portlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text"> Ingrese los detalles del recurso</h3>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right m-form--state" method="POST" action="{{ route('resources.update', [$resource]) }}"
                enctype="multipart/form-data">
                @csrf @method('PATCH')
                <div class="form-group m-form__group @if($errors->has('name')) has-danger @endif">
                    <label for="name"> Área:</label>
                    <input id="name" type="text" class="form-control col-lg-3" value="{{$resource->name}}"
                        name="name" placeholder="Ingrese Área"> @if($errors->has('name'))
                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group m-form__group @if($errors->has('abbr')) has-danger @endif">
                    <label for="abbr"> Abreviación:</label>
                    <input type="abbr" class="form-control col-lg-3" value="{{$resource->abbr}}" name="abbr" aria-describedby="emailHelp"
                        placeholder="Ingrese abreviación"> @if($errors->has('abbr'))
                    <div class="form-control-feedback">{{ $errors->first('abbr') }}</div>
                    @endif
                </div>
                <div class="form-group m-form__group @if($errors->has('employees')) has-danger @endif">
                    <label for="employees"> Número de Empleados:</label>
                    <input class="form-control col-lg-3" name="employees" type="employees" value="{{$resource->employees}}" placeholder="Ingrese el número de empleados de esta área"> @if($errors->has('employees'))
                    <div class="form-control-feedback">{{ $errors->first('employees') }}</div>
                    @endif
                </div>
                <div class="form-group m-form__group @if($errors->has('price')) has-danger @endif">
                    <label for="price"> Precio:</label>
                    <input type="price" class="form-control col-lg-3" name="price" value="{{$resource->price}}" placeholder="Ingrese precio por hora"> @if($errors->has('price'))
                    <div class="form-control-feedback">{{ $errors->first('price') }}</div>
                    @endif
                </div>
                <div class="form-group m-form__group @if($errors->has('space_id')) has-danger @endif">
                    <label for="space_id">Espacio en ClickUp:</label>
                    <select class="form-control selectpicker col-lg-4" name="space_id" id="space_id">
                        <option value="">ClickUp Space</option>
                        @foreach($spaces as $space)
                        <option value="{{$space['id']}}" @if($space['id']=== $resource->space_id) selected @endif>{{$space['name']}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('space_id'))
                    <div class="form-control-feedback">{{ $errors->first('status_id') }}</div>
                    @endif
                </div>
                <div class="m-form__actions">
                    <a href="{{ route("resources.index")}}" class="btn btn-metal">Cancelar</a>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection @push('scripts')
<script>
    $('.space_id').select2({
        placeholder: "ClickUp Space"
    });
</script>
@endpush