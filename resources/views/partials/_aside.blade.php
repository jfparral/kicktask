<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500"  >
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <!-- Dashboard -->
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="/" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                            <span class="m-menu__link-badge">
                                <span class="m-badge m-badge--danger">
                                    2
                                </span>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <!-- Clientes [tag] -->
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Clientes
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <!-- Usuarios -->
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="{{route('users.index')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Usuarios
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <!-- Clientes -->
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="{{route('clients.index')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-suitcase"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Clientes
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <!-- Cotizaciones [tag] -->
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Cotizaciones
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <!-- Cotizaciones -->
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="{{route('quotes.index')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-interface-4"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Cotizaciones
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <!-- Solicitudes [tag] -->
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Solicitudes
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <!-- Solicitudes -->
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="{{route('requirements.index')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-list-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Solicitudes
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <!-- Components [tag] -->
            <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Tareas
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                    <a  href="{{route('tasks.index')}}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-clipboard"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Tareas
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
                <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                    <a  href="{{route('tasks.calendar')}}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-calendar-2"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Calendario de Tareas
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
                <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                    <a  href="{{route('providers.index')}}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-user-settings"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Tareas de Proveedores
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    Recursos
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                <a  href="{{route('resources.index')}}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-puzzle"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Recursos
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            
            <!-- Settings [tag] -->
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">General</h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
                <i.m-menu__section-icon.flaticon-more></i>
            </li>
            <!-- Settings -->
            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                <a  href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-interface-9"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">    
                            <span class="m-menu__link-text">
                                Configuración
                            </span>
                        </span>
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">
                                    Sistem
                                </span>
                            </span>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="statuses" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Estados
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="sources" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Fuentes
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true" >
                            <a href="types" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Tipos
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>