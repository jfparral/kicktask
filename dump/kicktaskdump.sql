CREATE DATABASE  IF NOT EXISTS `kicktask` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kicktask`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: kicktask
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_users`
--

DROP TABLE IF EXISTS `client_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_users_client_id_foreign` (`client_id`),
  KEY `client_users_user_id_foreign` (`user_id`),
  CONSTRAINT `client_users_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `client_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_users`
--

LOCK TABLES `client_users` WRITE;
/*!40000 ALTER TABLE `client_users` DISABLE KEYS */;
INSERT INTO `client_users` VALUES (1,1,2),(2,1,3),(3,1,4),(4,1,5),(5,1,6);
/*!40000 ALTER TABLE `client_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legal_id` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clickup_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comercial_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Addidas','225 Freeda ParkEast Eleazarborough, DC 42508-5533','ilueilwitz@hammes.com','1682980475215','1111111111','logos/jIa5Ppa781eyOW6rQd7U4NKWX6eW4VBDScwc9DtN.jpeg',NULL,NULL,NULL,'ADIDDAS'),(2,'Beat','Guayaquil','jp@wekickads.com','1234567890123','1564861213','logos/MzA7U1zOJ0RIB0w4Suyea0ygSvGlh9WqjJhA7x6p.webp',NULL,NULL,NULL,'Beat');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `managements`
--

DROP TABLE IF EXISTS `managements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `managements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `quote_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `managements_task_id_foreign` (`task_id`),
  KEY `managements_quote_id_foreign` (`quote_id`),
  KEY `managements_client_id_foreign` (`client_id`),
  KEY `managements_status_id_foreign` (`status_id`),
  CONSTRAINT `managements_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `managements_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`),
  CONSTRAINT `managements_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `managements_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `managements`
--

LOCK TABLES `managements` WRITE;
/*!40000 ALTER TABLE `managements` DISABLE KEYS */;
/*!40000 ALTER TABLE `managements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2013_03_09_175342_role',1),(2,'2014_10_12_000000_create_users_table',1),(3,'2014_10_12_100000_create_password_resets_table',1),(4,'2018_03_09_175029_clients',1),(5,'2018_03_09_175030_client_user',1),(6,'2018_03_09_175148_resource',1),(7,'2018_03_09_175149_services',1),(8,'2018_03_09_175211_service_detail',1),(9,'2018_03_09_175235_source',1),(10,'2018_03_09_175321_type',1),(11,'2018_03_09_175409_status',1),(12,'2018_03_09_175410_requirement',1),(13,'2018_03_09_175411_quote',1),(14,'2018_03_09_175412_task',1),(15,'2018_03_09_175430_task_version',1),(16,'2018_03_09_175520_setting',1),(17,'2018_03_09_175607_status_type',1),(18,'2018_03_09_175641_management',1),(19,'2018_03_09_175925_quote_detail',1),(20,'2018_04_09_175734_order',1),(21,'2018_04_09_175750_order_detail',1),(22,'2018_04_16_153006_create_jobs_table',2),(23,'2018_04_23_210408_provider',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax` int(11) NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `tax_total` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_details_order_id_foreign` (`order_id`),
  CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legal_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit_day` int(11) NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `tax` int(11) NOT NULL,
  `tax_total` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `quote_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_quote_id_foreign` (`quote_id`),
  KEY `orders_status_id_foreign` (`status_id`),
  CONSTRAINT `orders_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`),
  CONSTRAINT `orders_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providers`
--

DROP TABLE IF EXISTS `providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `quote_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `provider_id` int(10) unsigned NOT NULL,
  `quote_detail_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `providers_provider_id_foreign` (`provider_id`),
  KEY `providers_status_id_foreign` (`status_id`),
  KEY `providers_quote_id_foreign` (`quote_id`),
  KEY `providers_quote_detail_id_foreign_idx` (`quote_detail_id`),
  CONSTRAINT `providers_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `users` (`id`),
  CONSTRAINT `providers_quote_detail_id_foreign` FOREIGN KEY (`quote_detail_id`) REFERENCES `quotes_details` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `providers_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`),
  CONSTRAINT `providers_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providers`
--

LOCK TABLES `providers` WRITE;
/*!40000 ALTER TABLE `providers` DISABLE KEYS */;
INSERT INTO `providers` VALUES (1,'Proveedor','sadasdas',NULL,148.47,7,1,3,36,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(2,'Proveedor','Prueba 2 con version 2.',NULL,822.41,7,1,3,33,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(3,'Proveedor','asdagfdgfds',NULL,141.45,7,1,3,37,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(4,'Proveedor','sadasdas',NULL,148.47,7,1,3,36,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(5,'Proveedor','Prueba 2 con version 2.',NULL,822.41,7,1,3,33,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(6,'Proveedor','asdagfdgfds',NULL,141.45,7,1,3,37,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(7,'Proveedor','sadasdas',NULL,148.47,7,1,3,36,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(8,'Proveedor','Prueba 2 con version 2.',NULL,822.41,7,1,3,33,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(9,'Proveedor','asdagfdgfds',NULL,141.45,7,1,3,37,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(10,'Proveedor','sadasdas',NULL,148.47,7,1,3,36,'2018-04-27 01:36:18','2018-04-27 01:36:18'),(11,'Proveedor','Prueba 2 con version 2.',NULL,822.41,7,1,3,33,'2018-04-27 01:36:18','2018-04-27 01:36:18'),(12,'Proveedor','asdagfdgfds',NULL,141.45,7,1,3,37,'2018-04-27 01:36:18','2018-04-27 01:36:18'),(13,'Proveedor','sadasdas',NULL,148.47,7,1,3,36,'2018-04-27 01:51:29','2018-04-27 01:51:29'),(14,'Proveedor','Prueba 2 con version 2.',NULL,822.41,7,1,3,33,'2018-04-27 01:51:29','2018-04-27 01:51:29'),(15,'Proveedor','asdagfdgfds',NULL,141.45,7,1,3,37,'2018-04-27 01:51:29','2018-04-27 01:51:29');
/*!40000 ALTER TABLE `providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comercial_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_legal_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `exp_date` date DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_days` int(11) DEFAULT NULL,
  `contact_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `tax_total` decimal(8,2) DEFAULT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `requirement_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `email_token` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotes_client_id_foreign` (`client_id`),
  KEY `quotes_contact_id_foreign` (`contact_id`),
  KEY `quotes_requirement_id_foreign` (`requirement_id`),
  KEY `quotes_status_id_foreign` (`status_id`),
  KEY `quotes_user_id_foreign_idx` (`user_id`),
  CONSTRAINT `quotes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `quotes_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `users` (`id`),
  CONSTRAINT `quotes_requirement_id_foreign` FOREIGN KEY (`requirement_id`) REFERENCES `requirements` (`id`),
  CONSTRAINT `quotes_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `quotes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes`
--

LOCK TABLES `quotes` WRITE;
/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
INSERT INTO `quotes` VALUES (7,'ADIDDAS','Addidas','ilueilwitz@hammes.com','1111111111','225 Freeda ParkEast Eleazarborough, DC 42508-5533','1682980475215','<p><u>Prueba</u>, <b style=\"background-color: rgb(255, 255, 0);\">términos legales</b>.</p>','2018-04-26','Forma de pago inmediata.',NULL,'Jerad Treutel','arowe@example.net',1391.03,12,166.92,10,1557.95,1,3,2,'2018-04-02 19:29:03','2018-04-27 02:54:06',2,'5ae095cb0e931'),(8,'ADIDDAS','Addidas','ilueilwitz@hammes.com','1111111111','225 Freeda ParkEast Eleazarborough, DC 42508-5533','1682980475215','<p>Terminos legales de prueba</p>','2018-04-27','Pago a plazo 15 días.',NULL,'Jerad Treutel','arowe@example.net',0.00,12,0.00,1,0.00,1,3,2,'2018-04-04 22:18:28','2018-04-25 00:49:56',2,NULL),(10,'Beat','Beat','jp@wekickads.com','1564861213','Guayaquil','1234567890123','<p>Prueba de cotización.</p>','2018-04-27','Prueba.',NULL,'Celestino Sawayn IV','angela.balistreri@example.org',0.00,12,0.00,1,0.00,2,4,3,'2018-04-17 03:04:08','2018-04-25 00:50:01',2,NULL),(14,'ADIDDAS','Addidas','ilueilwitz@hammes.com','1111111111','225 Freeda ParkEast Eleazarborough, DC 42508-5533','1682980475215','<p>prueba de cotizacion</p>','2018-04-30','pago, forma.',NULL,'Mortimer Price','mharvey@example.com',272.40,12,32.69,1,305.09,1,6,2,'2018-04-24 22:48:44','2018-04-25 00:50:05',2,NULL);
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes_details`
--

DROP TABLE IF EXISTS `quotes_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource_id` int(10) unsigned DEFAULT NULL,
  `dealer_id` int(10) unsigned DEFAULT NULL,
  `hours` decimal(8,2) DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  `gain_percent` decimal(8,2) NOT NULL,
  `gain` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `quote_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quotes_details_quote_id_foreign` (`quote_id`),
  KEY `quotes_details_dealer_id_foreign` (`dealer_id`),
  KEY `quotes_details_resource_id_foreign` (`resource_id`),
  KEY `quotes_detail_type_id_foreign_idx` (`type_id`),
  CONSTRAINT `quotes_detail_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `quotes_details_dealer_id_foreign` FOREIGN KEY (`dealer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `quotes_details_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`),
  CONSTRAINT `quotes_details_resource_id_foreign` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes_details`
--

LOCK TABLES `quotes_details` WRITE;
/*!40000 ALTER TABLE `quotes_details` DISABLE KEYS */;
INSERT INTO `quotes_details` VALUES (1,'Prueba','Prueba interna.',1,NULL,12.00,120.00,15.00,18.00,138.00,'intern',1,7,'2018-04-02 20:43:48','2018-04-02 20:43:48','5ac2a258c8eb0',1),(3,'Prueba2','Prueba2',2,NULL,10.00,100.00,11.00,11.00,111.00,'intern',1,7,'2018-04-03 00:42:22','2018-04-03 00:42:22','5ac2a26c4d3f6',1),(5,'Prueba3','Prueba3',4,NULL,9.00,90.00,13.00,11.70,101.70,'intern',1,7,'2018-04-03 00:45:13','2018-04-03 00:45:13','5ac2a27e08fae',1),(6,'Pruebav2','Prueba interna v2.',2,NULL,12.00,120.00,20.00,24.00,138.00,'intern',2,7,'2018-04-03 02:56:47','2018-04-03 02:56:47','5ac2a258c8eb0',1),(7,'asdadas','sadasdas',NULL,7,NULL,123.00,12.00,14.76,137.76,'extern',1,7,'2018-04-03 20:20:08','2018-04-03 20:20:08','5ac39ba8b7c64',1),(8,'Pruebav3','Prueba interna v2.',2,NULL,12.00,120.00,20.00,24.00,144.00,'intern',3,7,'2018-04-04 00:51:42','2018-04-04 00:51:42','5ac2a258c8eb0',1),(9,'Pruebav2','Pruebav2',2,NULL,10.00,100.00,11.00,11.00,111.00,'intern',2,7,'2018-04-04 00:52:33','2018-04-04 00:52:33','5ac2a26c4d3f6',1),(10,'Prueba3v2','Prueba3v2',4,NULL,9.00,90.00,13.00,11.70,101.70,'intern',2,7,'2018-04-04 00:53:30','2018-04-04 00:53:30','5ac2a27e08fae',1),(12,'Prueba3v2','Prueba3v2',4,NULL,9.00,90.00,14.00,12.15,102.15,'intern',3,7,'2018-04-04 01:30:19','2018-04-04 01:30:19','5ac2a27e08fae',1),(13,'Pruebav2','Pruebav2',2,NULL,10.00,100.00,14.00,13.50,113.50,'intern',3,7,'2018-04-04 01:30:46','2018-04-04 01:30:46','5ac2a26c4d3f6',1),(14,'Pruebav3','Prueba interna v2.',2,NULL,13.00,125.00,21.00,25.70,150.70,'intern',4,7,'2018-04-04 20:44:19','2018-04-04 20:44:19','5ac2a258c8eb0',1),(15,'Pruebav3','Prueba interna v2.',2,NULL,14.00,135.00,22.00,29.16,164.16,'intern',5,7,'2018-04-04 20:44:50','2018-04-04 20:44:50','5ac2a258c8eb0',1),(16,'Pruebav3','Prueba interna v2.',2,NULL,12.00,115.00,35.00,40.46,155.46,'intern',6,7,'2018-04-04 20:46:43','2018-04-04 20:46:43','5ac2a258c8eb0',1),(17,'Pruebav3','Prueba interna v2.sfasfsadsa',2,NULL,12.00,120.00,35.00,42.00,162.00,'intern',7,7,'2018-04-04 20:47:31','2018-04-04 20:47:31','5ac2a258c8eb0',1),(18,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,12.00,120.00,35.00,42.00,162.00,'intern',8,7,'2018-04-04 20:48:03','2018-04-04 20:48:03','5ac2a258c8eb0',1),(19,'Pruebav2','Pruebav2',2,NULL,10.00,100.00,14.00,14.15,114.15,'intern',4,7,'2018-04-04 20:48:47','2018-04-04 20:48:47','5ac2a26c4d3f6',1),(20,'Pruebav2','Pruebav2',2,NULL,15.00,150.00,14.00,21.23,171.23,'intern',5,7,'2018-04-04 20:49:21','2018-04-04 20:49:21','5ac2a26c4d3f6',1),(21,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,0.00,0.00,0.00,0.00,0.00,'intern',9,7,'2018-04-04 20:51:55','2018-04-04 20:51:55','5ac2a258c8eb0',1),(22,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,10.00,100.00,15.00,15.18,115.18,'intern',10,7,'2018-04-04 20:52:34','2018-04-04 20:52:34','5ac2a258c8eb0',1),(23,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,10.00,100.00,15.00,15.25,115.25,'intern',11,7,'2018-04-04 20:53:23','2018-04-04 20:53:23','5ac2a258c8eb0',1),(24,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,10.00,100.00,15.00,15.30,115.30,'intern',12,7,'2018-04-04 21:01:25','2018-04-04 21:01:25','5ac2a258c8eb0',1),(25,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,10.00,100.00,25.86,25.86,125.86,'intern',13,7,'2018-04-04 21:04:26','2018-04-04 21:04:26','5ac2a258c8eb0',1),(26,'Pruebav3','Prueba interna v2.sfasfsadsad',3,NULL,10.00,100.00,25.86,25.86,125.86,'intern',14,7,'2018-04-04 21:04:40','2018-04-04 21:04:40','5ac2a258c8eb0',1),(27,'Pruebav3','Prueba interna v2.sfasfsadsad',4,NULL,10.00,100.00,25.86,25.86,125.86,'intern',15,7,'2018-04-04 21:04:52','2018-04-04 21:04:52','5ac2a258c8eb0',1),(28,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,10.00,100.00,25.86,25.86,125.86,'intern',16,7,'2018-04-04 21:06:04','2018-04-04 21:06:04','5ac2a258c8eb0',1),(29,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,12.00,115.00,25.86,29.74,144.74,'intern',17,7,'2018-04-04 21:15:55','2018-04-04 21:15:55','5ac2a258c8eb0',1),(30,'Pruebav3','Prueba interna v2.sfasfsadsad',2,NULL,12.50,125.00,25.86,32.33,157.33,'intern',18,7,'2018-04-04 21:18:20','2018-04-04 21:18:20','5ac2a258c8eb0',1),(31,'asda','sadasdas',NULL,7,NULL,123.15,20.56,25.32,137.91,'extern',2,7,'2018-04-04 21:54:59','2018-04-04 21:54:59','5ac39ba8b7c64',1),(32,'sadasd','Prueba 2.',NULL,7,NULL,526.78,56.12,295.63,822.41,'extern',1,7,'2018-04-04 21:55:33','2018-04-04 21:55:33','5ac503856e5e1',1),(33,'asdad','Prueba 2 con version 2.',NULL,7,NULL,526.78,56.12,295.63,822.41,'extern',2,7,'2018-04-04 21:55:54','2018-04-04 21:55:54','5ac503856e5e1',1),(34,'sdad','sadsda',1,NULL,12.00,120.00,12.00,14.40,134.40,'intern',1,14,'2018-04-24 22:49:46','2018-04-24 22:49:46','5adf6e3a8ab02',1),(35,'asda','sdsdadadsad',NULL,7,NULL,120.00,15.00,18.00,138.00,'extern',1,14,'2018-04-24 22:50:09','2018-04-24 22:50:09','5adf6e51912d0',1),(36,'asdsa','sadasdas',NULL,7,NULL,123.15,20.56,25.32,148.47,'extern',3,7,'2018-04-25 02:56:33','2018-04-25 02:56:33','5ac39ba8b7c64',6),(37,'prueba name ext','asdagfdgfds',NULL,7,NULL,123.00,15.00,18.45,141.45,'extern',1,7,'2018-04-25 21:23:36','2018-04-25 21:23:36','5ae0ab88491d0',6);
/*!40000 ALTER TABLE `quotes_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requirements`
--

DROP TABLE IF EXISTS `requirements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date NOT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `source_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requirements_user_id_foreign` (`user_id`),
  KEY `requirements_contact_id_foreign` (`contact_id`),
  KEY `requirements_source_id_foreign` (`source_id`),
  KEY `requirements_client_id_foreign` (`client_id`),
  KEY `requirements_status_id_foreign` (`status_id`),
  CONSTRAINT `requirements_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `requirements_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `users` (`id`),
  CONSTRAINT `requirements_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`),
  CONSTRAINT `requirements_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `requirements_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requirements`
--

LOCK TABLES `requirements` WRITE;
/*!40000 ALTER TABLE `requirements` DISABLE KEYS */;
INSERT INTO `requirements` VALUES (2,'<p><b>Prueba.</b></p>','2018-03-27',1,2,6,1,4,'2018-03-23 20:14:59','2018-04-04 23:10:41'),(3,'<p>Prueba.</p>','2018-04-27',1,2,4,2,5,'2018-04-17 03:04:01','2018-04-17 03:04:01');
/*!40000 ALTER TABLE `requirements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbr` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employees` int(11) NOT NULL,
  `space_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,'Desarrollo','Web',3,'453883',10.00,NULL,'2018-04-10 00:34:29'),(2,'Diseño','DIseño',2,'453840',12.00,NULL,'2018-04-09 20:24:47'),(3,'Redacción','Redac',3,'453884',12.00,NULL,'2018-04-09 20:24:57'),(4,'Pauta','Pauta',1,'453886',14.00,NULL,'2018-04-09 20:27:02');
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'cliente','2018-03-23 19:51:30','2018-03-23 19:51:30'),(2,'admin','2018-03-23 19:51:30','2018-03-23 19:51:30'),(3,'contact','2018-03-23 19:51:30','2018-03-23 19:51:30'),(4,'provider','2018-03-23 19:51:30','2018-03-23 19:51:30');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_details`
--

DROP TABLE IF EXISTS `service_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours` int(11) NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_details_service_id_foreign` (`service_id`),
  CONSTRAINT `service_details_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_details`
--

LOCK TABLES `service_details` WRITE;
/*!40000 ALTER TABLE `service_details` DISABLE KEYS */;
INSERT INTO `service_details` VALUES (4,'Desarrollo','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',12,11,'2018-04-26 20:27:31','2018-04-26 20:27:31'),(5,'Prueba 2','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',12,10,'2018-04-26 03:41:50','2018-04-26 03:41:50'),(6,'Prueba 3','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',12,10,'2018-04-26 03:41:50','2018-04-26 03:41:50'),(7,'Prueba 4','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',12,10,'2018-04-26 03:41:50','2018-04-26 03:41:50'),(8,'Prueba 5','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',15,10,'2018-04-26 03:41:50','2018-04-27 00:43:02');
/*!40000 ALTER TABLE `service_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `resource_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `services_client_id_foreign` (`client_id`),
  KEY `services_resource_id_foreign` (`resource_id`),
  CONSTRAINT `services_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `services_resource_id_foreign` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (10,1,2,'2018-04-26 03:41:49','2018-04-26 03:41:49'),(11,1,1,'2018-04-26 20:27:31','2018-04-26 20:27:31');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources`
--

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
INSERT INTO `sources` VALUES (1,'Facebook'),(2,'Instagram'),(3,'Twitter'),(4,'Email'),(5,'Llamada celular'),(6,'Llamada convencional');
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_types`
--

DROP TABLE IF EXISTS `status_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('requirements','tasks','managements','quotes') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status_types_status_id_foreign` (`status_id`),
  CONSTRAINT `status_types_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_types`
--

LOCK TABLES `status_types` WRITE;
/*!40000 ALTER TABLE `status_types` DISABLE KEYS */;
INSERT INTO `status_types` VALUES (1,'requirements',1),(2,'requirements',5),(3,'requirements',6),(4,'requirements',7),(5,'tasks',1),(6,'tasks',5),(7,'tasks',6),(8,'tasks',7),(9,'managements',1),(10,'managements',5),(11,'managements',6),(12,'managements',7),(13,'quotes',1),(14,'quotes',2),(15,'quotes',3),(16,'quotes',4),(17,'quotes',8),(18,'tasks',9),(19,'quotes',9),(20,'quotes',10);
/*!40000 ALTER TABLE `status_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'Abierta'),(2,'Aprobada'),(3,'Cotizada'),(4,'Cotizar'),(5,'Detenida'),(6,'Proceso'),(7,'Cerrada'),(8,'Borrador'),(9,'Enviada'),(10,'Rechazada');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_versions`
--

DROP TABLE IF EXISTS `task_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task_versions_task_id_foreign` (`task_id`),
  CONSTRAINT `task_versions_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_versions`
--

LOCK TABLES `task_versions` WRITE;
/*!40000 ALTER TABLE `task_versions` DISABLE KEYS */;
INSERT INTO `task_versions` VALUES (1,'Prueba',1,1,'2018-03-29 20:18:18','2018-03-29 20:18:18'),(2,'<b><u>Prueba</u></b>',2,1,'2018-03-29 21:15:48','2018-03-29 21:15:48'),(3,'Prueba',3,1,'2018-04-10 03:47:15','2018-04-10 03:47:15'),(4,'Prueba para botón de enviada.',1,2,'2018-04-10 20:45:37','2018-04-10 20:45:37'),(5,'Prueba de version',4,1,'2018-04-20 22:16:51','2018-04-20 22:16:51'),(7,'Prueba de versiojkljkl',5,1,'2018-04-24 20:53:00','2018-04-24 20:53:00'),(8,'Prueba de versiojkljkl version 6 La historia del chocolate en España es una parte de la historia culinaria hispánica comprendida desde el siglo xvi, cuando los españoles conocieron el cacao (Theobroma cacao) mesoamericano al comenzar la colonización de América, hasta el presente. Cuando Hernán Cortés viajó a México fue acompañado por el cisterciense fray Jerónimo Aguilar que envió al abad del monasterio de Piedra el primer cacao junto a la receta para cocinarlo. Tras la Conquista de México, el cacao viaja como mercancía en barco desde un puerto de Nueva España, rumbo a las costas españolas. Este primer viaje a Europa ocurre por primera vez en algún momento indeterminado de la década de 1520. No fue hasta el siglo xvii cuando sale regularmente desde el puerto de Veracruz, abriendo una ruta comercial marítima que abastecería la nueva demanda de España, y posteriormente de Europa.\n\nLa introducción de este ingrediente en las costumbres culinarias españolas fue ciertamente inmediato, comparado con la de otros ingredientes traídos de América, y su popularidad y aceptación en todos los estamentos de la sociedad española alcanzó niveles muy elevados ya a finales del siglo xvi. Desde sus comienzos, el chocolate fue considerado por los españoles como una bebida y permaneció en ese concepto hasta principios del siglo xx.',6,1,'2018-04-24 20:53:35','2018-04-24 20:53:35'),(9,'Prueba interna v2.sfasfsadsad',1,3,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(10,'Pruebav2',1,4,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(11,'Prueba3v2',1,5,'2018-04-27 01:29:06','2018-04-27 01:29:06'),(12,'Prueba interna v2.sfasfsadsad',1,6,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(13,'Pruebav2',1,7,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(14,'Prueba3v2',1,8,'2018-04-27 01:31:37','2018-04-27 01:31:37'),(15,'Prueba interna v2.sfasfsadsad',1,9,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(16,'Pruebav2',1,10,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(17,'Prueba3v2',1,11,'2018-04-27 01:33:37','2018-04-27 01:33:37'),(18,'Prueba interna v2.sfasfsadsad',1,12,'2018-04-27 01:36:17','2018-04-27 01:36:17'),(19,'Pruebav2',1,13,'2018-04-27 01:36:17','2018-04-27 01:36:17'),(20,'Prueba3v2',1,14,'2018-04-27 01:36:17','2018-04-27 01:36:17'),(21,'Prueba interna v2.sfasfsadsad',1,15,'2018-04-27 01:51:28','2018-04-27 01:51:28'),(22,'Pruebav2',1,16,'2018-04-27 01:51:28','2018-04-27 01:51:28'),(23,'Prueba3v2',1,17,'2018-04-27 01:51:29','2018-04-27 01:51:29');
/*!40000 ALTER TABLE `task_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date DEFAULT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `requirement_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `resource_id` int(10) unsigned NOT NULL,
  `quote_id` int(10) unsigned DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `clickup_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delay_hour` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_requirement_id_foreign` (`requirement_id`),
  KEY `tasks_resource_id_foreign` (`resource_id`),
  KEY `tasks_quote_id_foreign` (`quote_id`),
  KEY `tasks_client_id_foreign` (`client_id`),
  KEY `tasks_status_id_foreign` (`status_id`),
  CONSTRAINT `tasks_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `tasks_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`),
  CONSTRAINT `tasks_requirement_id_foreign` FOREIGN KEY (`requirement_id`) REFERENCES `requirements` (`id`),
  CONSTRAINT `tasks_resource_id_foreign` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`),
  CONSTRAINT `tasks_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'Crear Web','2018-03-31',9,2,1,1,NULL,1,'2018-03-29 20:18:18','2018-04-27 03:24:51',NULL,'2018-04-27 05:05:00.000000'),(2,'Tarea2','2018-04-20',9,2,4,3,NULL,1,'2018-04-10 20:45:36','2018-04-10 23:52:12','gwdy','2011-01-26 14:30:00.000000'),(3,'Pruebav3',NULL,9,2,1,2,7,1,'2018-04-27 01:29:06','2018-04-27 03:26:39','hj4j','2018-04-28 00:12:00.000000'),(4,'prueba name ext','2018-04-20',9,2,1,2,7,1,'2018-04-27 01:29:06','2018-04-27 03:30:44','hj4k','2018-04-28 00:12:00.000000'),(5,'Prueba3v2',NULL,1,2,1,4,7,1,'2018-04-27 01:29:06','2018-04-27 01:29:06',NULL,NULL),(6,'Pruebav3',NULL,1,2,1,2,7,1,'2018-04-27 01:31:37','2018-04-27 01:31:37',NULL,NULL),(7,'Pruebav2',NULL,1,2,1,2,7,1,'2018-04-27 01:31:37','2018-04-27 01:31:37',NULL,NULL),(8,'Prueba3v2',NULL,1,2,1,4,7,1,'2018-04-27 01:31:37','2018-04-27 01:31:37',NULL,NULL),(9,'Pruebav3',NULL,1,2,1,2,7,1,'2018-04-27 01:33:37','2018-04-27 01:33:37',NULL,NULL),(10,'Pruebav2',NULL,1,2,1,2,7,1,'2018-04-27 01:33:37','2018-04-27 01:33:37',NULL,NULL),(11,'Prueba3v2',NULL,1,2,1,4,7,1,'2018-04-27 01:33:37','2018-04-27 01:33:37',NULL,NULL),(12,'Pruebav3',NULL,1,2,1,2,7,1,'2018-04-27 01:36:17','2018-04-27 01:36:17',NULL,NULL),(13,'Pruebav2',NULL,1,2,1,2,7,1,'2018-04-27 01:36:17','2018-04-27 01:36:17',NULL,NULL),(14,'Prueba3v2',NULL,1,2,1,4,7,1,'2018-04-27 01:36:17','2018-04-27 01:36:17',NULL,NULL),(15,'Pruebav3',NULL,1,2,1,2,7,1,'2018-04-27 01:51:28','2018-04-27 01:51:28',NULL,NULL),(16,'Pruebav2',NULL,1,2,1,2,7,1,'2018-04-27 01:51:28','2018-04-27 01:51:28',NULL,NULL),(17,'Prueba3v2',NULL,1,2,1,4,7,1,'2018-04-27 01:51:29','2018-04-27 01:51:29',NULL,NULL);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'Actualización'),(2,'Mantenimiento'),(3,'Error'),(4,'Ampliación'),(5,'Normal'),(6,'Externo');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legal_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Prof. Misael Ritchie','walker.alicia@example.org','$2y$10$Vp5xVhUxMOAkBRvJHevC2esoBdSWv/./p0B5pksRA11wB5YUZn.r2','272-386-8383 x437','6584347324912','452 Terry Ford\nJackmouth, AZ 69422',2,'enJadqGahcWJgm22BswwEa885NfFz87YBZAXmWh2YnNSVnCukhjBJ7DU2w3t','2018-03-23 19:45:39','2018-03-23 19:45:39'),(3,'Jerad Treutel','arowe@example.net','$2y$10$LoGuZKKegolnK0UWC/kA4u71vSleGIDMbay1p3rWdl9kSfAqXLNoK','(932) 751-6165','2931167578560','4283 Hodkiewicz Station\nPort Haylee, RI 48700-3728',1,'a5xvYpLaKa','2018-03-23 19:51:55','2018-03-23 19:51:55'),(4,'Celestino Sawayn IV','angela.balistreri@example.org','$2y$10$Vp5xVhUxMOAkBRvJHevC2esoBdSWv/./p0B5pksRA11wB5YUZn.r2','+18306629392','4632722968649','197 Geovany Locks\nSouth Erica, UT 99239',2,'W3g5S8TiKG','2018-03-23 19:51:56','2018-03-23 19:51:56'),(5,'Edgar Beer','carissa.eichmann@example.org','$2y$10$fEXOXEoC/e9brvlwoakkLenTvu3zdhaIlohiNA0k6Mc6j2GDAkYqq','785-484-1269','6968813067402','36350 Jazlyn Curve\nBradtkeview, MI 41746',1,'julhF0NdIK','2018-03-23 19:51:56','2018-03-23 19:51:56'),(6,'Mortimer Price','mharvey@example.com','$2y$10$1Jm87l0khcIWTGcwFTEmRe.mwmSb665e2vMTv6qKrpeC7MYojOi/O','+1 (760) 345-4100','9189477292567','284 Grant Harbors Apt. 698\nPort Shanie, AK 95345-1212',1,'bpF5ftHaij','2018-03-23 19:51:56','2018-03-23 19:51:56'),(7,'Proveedor','provider@example.org','$2y$10$Qtpuf84UjLHfGdQ3KAi9D.GFWRB1FMmpPGuf1G.N8tnwDcL8udKee','1564861213','1234567890','Guayaquil',4,NULL,'2018-03-29 23:18:51','2018-03-29 23:18:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 17:18:58
