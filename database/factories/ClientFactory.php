<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'legal_id' => $faker->ean13,
        'phone' => '1111111111',
        'email' => $faker->email
    ];
});
