<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuoteDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description');
            $table->decimal('cost',8,2);
            $table->integer('resource_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('dealer_id')->unsigned()->nullable();
            $table->decimal('hours',8,2)->nullable();
            $table->string('group');
            $table->decimal('gain_percent', 8, 2);
            $table->decimal('gain', 8, 2);
            $table->decimal('total', 8, 2);
            $table->string('type');
            $table->integer('version');
            $table->integer('quote_id')->unsigned();
            $table->foreign('resource_id')
                ->references('id')->on('resources');
            $table->foreign('dealer_id')
                ->references('id')->on('users');
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->foreign('type_id')
                ->references('id')->on('types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes_details');
    }
}
