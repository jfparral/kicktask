<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Requirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->date('due_date');
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('source_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('contact_id')
                ->references('id')->on('users');
            $table->foreign('source_id')
                ->references('id')->on('sources');
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirements');
    }
}
