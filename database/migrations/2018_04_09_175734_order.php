<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('legal_id');
            $table->string('payment');
            $table->integer('credit_day');
            $table->decimal('subtotal',8,2);
            $table->integer('tax');
            $table->decimal('tax_total',8,2);
            $table->decimal('total',8,2);
            $table->text('terms');
            $table->integer('quote_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
