<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Management extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('task_id')->unsigned();
            $table->integer('quote_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->foreign('task_id')
                ->references('id')->on('tasks');
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managements');
    }
}
