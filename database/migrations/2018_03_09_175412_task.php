<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Task extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('due_date');
            $table->datetime('delay_hour')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('requirement_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('resource_id')->unsigned();
            $table->integer('quote_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned();
            $table->foreign('requirement_id')
                ->references('id')->on('requirements');
            $table->foreign('resource_id')
                ->references('id')->on('resources');
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
