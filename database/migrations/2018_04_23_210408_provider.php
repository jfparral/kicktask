<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Provider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_provider');
            $table->string('description');
            $table->date('due_date')->nullable();
            $table->decimal('price',8,2);
            $table->integer('quote_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')
                ->references('id')->on('users');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
