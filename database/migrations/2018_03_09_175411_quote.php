<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Quote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comercial_name')->nullable();
            $table->string('client_name');
            $table->string('client_email');
            $table->string('client_phone');
            $table->string('client_address');
            $table->string('client_legal_id');
            $table->text('terms')->nullable();
            $table->date('exp_date')->nullable();
            $table->string('payment')->nullable();
            $table->integer('credit_days')->nullable();
            $table->string('contact_name');
            $table->string('email_token')->nullable();
            $table->string('contact_email');
            $table->decimal('subtotal', 8, 2)->nullable();
            $table->integer('tax')->nullable();
            $table->decimal('tax_total', 8, 2)->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->decimal('total', 8, 2)->nullable();
            $table->integer('client_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('requirement_id')->unsigned();
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('contact_id')
                ->references('id')->on('users');
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('requirement_id')
                ->references('id')->on('requirements');
            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
