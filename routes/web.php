<?php

use App\Resource;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* View of quote for client */
Route::get('/view/{token}/{quote}','MailsController@view')->name('email.view');
Route::get('/quote/exit','MailsController@exit')->name('email.exit');
/* Route for mail sending */
Route::get('/send/{quote}', 'MailsController@send')->name('email.send');
Route::get('/send/accept/{quote}','MailsController@accept')->name('email.accept');
Route::post('/send/refuse/{quote}', 'MailsController@refuse')->name('email.refuse');
/* Route for pdf generation */
Route::get('/generate-pdf/{id}','PDFController@pdf_download')->name('email.pdf_download');
Route::get('/download/{id}', 'PDFController@download')->name('email.download');

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::view('/', 'example');
    /* Clients */
    Route::resource('clients', 'ClientsController');
    /* Services */
    Route::get('/{client}/services/create', 'ServicesController@create')->name('services.create');
    Route::post('/{client}/services/store', 'ServicesController@store')->name('services.store');
    Route::delete('/services/delete/{service}', 'ServicesController@destroy')->name('services.destroy');
    /*  Services Details */
    Route::patch('/services_details/update/{service_detail}','ServicesDetailsController@update')->name('services_detail.update');
    Route::delete('/services_details/delete/{service_detail}','ServicesDetailsController@destroy')->name('services_detail.destroy');
    /* Requirements */
    Route::get('/requirements', 'RequirementsController@index')->name('requirements.index');
    Route::get('/requirements/show/{requirement}', 'RequirementsController@show')->name('requirements.show');
    Route::resource('requirements', 'RequirementsController', ['except' => ['index', 'show']]);
    /* Users */
    Route::resource('users', 'UsersController');
    /* Resources */
    Route::resource('resources', 'ResourcesController', ['except' => ['show']]);
    /* Task */
    Route::get('/tasks/calendar','TasksController@calendar')->name('tasks.calendar');
    Route::post('/task/{requirement}/store', 'TasksController@store')->name('tasks.store');
    Route::get('/task/{task}/clickup','TasksController@Project_Task')->name('tasks.Project_Task');
    Route::post('task/{task}/project','TasksController@Project')->name('tasks.Project');
    Route::post('/task/{task}/postTask','TasksController@postTask')->name('tasks.postTask');
    Route::resource('tasks', 'TasksController', ['except' => ['create', 'store']]);
    /* Quotes */
    Route::post('/quotes/{requirement}/store', 'QuotesController@store')->name('quotes.store');
    Route::resource('quotes', 'QuotesController', ['except' => ['create', 'store']]);
    /* Quotes Details*/
    Route::post('/quotes_details/{quote}/store', 'Quotes_DetailsController@store')->name('quotes_details.store');
    Route::patch('/quotes_details/{quote_detail}/update', 'Quotes_DetailsController@update')->name('quotes_details.update');
    Route::delete('/quotes_details/{quote_detail}', 'Quotes_DetailsController@destroy')->name('quotes_details.destroy');
    /* Roles */
    Route::resource('roles', 'RolesController');

    /* Api */
    Route::get('/api/resource/{resource}', function (Resource $resource) {
        return $resource;
    });

    /* Statuses */
    Route::resource('statuses', 'StatusesController');

    /* Sources */
    Route::resource('sources', 'SourcesController');

    /* Types */
    Route::resource('types', 'TypesController');


    /* Providers */
    Route::resource('providers','ProvidersController', ['except' => ['create', 'store','edit']]);
});